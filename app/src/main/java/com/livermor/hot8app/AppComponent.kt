package com.livermor.hot8app

import android.content.Context
import android.content.SharedPreferences
import com.livermor.hot8app.data.preference.global.GlobalDataModule
import com.livermor.hot8app.data.preference.global.IGlobalData
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, GlobalDataModule::class))
interface AppComponent {
    fun getContext(): Context
    fun getApp(): App
    fun getPrefs(): SharedPreferences
    fun getGlobalData(): IGlobalData
}