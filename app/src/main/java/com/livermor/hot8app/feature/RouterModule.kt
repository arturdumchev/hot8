package com.livermor.hot8app.feature

import com.livermor.hot8app.feature.common.injection.learner.Learner
import dagger.Module
import dagger.Provides


@Module
class RouterModule(private val router: IRouter) {

    @Learner
    @Provides
    fun provideRouter(router: IRouter): IRouter = router
}