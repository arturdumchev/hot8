package com.livermor.hot8app.feature

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.choose_words.ChooseWordsFrag
import com.livermor.hot8app.feature.common.fragment.addFrag
import com.livermor.hot8app.feature.main_frag.MainFrag


class MainActivity : AppCompatActivity(), IRouter {

    private val mainFrag by lazy(N) { MainFrag() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showMainFrag()
    }

    override fun showMainFrag() {
        addFrag(mainFrag)
    }

    override fun showTraining() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showRepetition() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showChooseWords() {
        addFrag(ChooseWordsFrag())
    }

    private fun addFrag(frag: Fragment) {
        supportFragmentManager.addFrag(R.id.main_frame, frag)
    }
}
