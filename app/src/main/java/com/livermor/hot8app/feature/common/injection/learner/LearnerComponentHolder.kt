package com.livermor.hot8app.feature.common.injection.learner

import com.livermor.hot8app.feature.common.injection.ComponentHolder
import com.livermor.hot8app.feature.common.injection.feature.FeatureComponentHolder


object LearnerComponentHolder : ComponentHolder<LearnerComponent>() {

    override fun createComponent(): LearnerComponent =
            DaggerLearnerComponent.builder()
                    .featureComponent(FeatureComponentHolder.component)
                    .build()

}