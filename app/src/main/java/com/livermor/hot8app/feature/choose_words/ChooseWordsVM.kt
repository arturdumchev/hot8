package com.livermor.hot8app.feature.choose_words

import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.common.util.bs
import com.livermor.hot8app.common.util.eats
import com.livermor.hot8app.common.util.ps
import com.livermor.hot8app.domain.words_for_future.IWordsInteractor
import com.livermor.hot8app.feature.common.base.BaseVM
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*
import javax.inject.Inject

class ChooseWordsVM @Inject constructor(
        override val errorHandler: IErrorHandler,
        private val interactor: IWordsInteractor) : BaseVM(), IChooseWordsVM {

    private val wordsSubj: BehaviorSubject<List<Word>>
    private val autoSlide: BehaviorSubject<Unit>
    private val wordsAmountSubj: BehaviorSubject<Int>
    private val chosenWordsCache: MutableSet<Word>

    private val shouldGoNext: PublishSubject<Unit>

    init {
        wordsSubj = bs()
        autoSlide = bs()
        wordsAmountSubj = bs()
        shouldGoNext = ps()
        chosenWordsCache = HashSet()
    }

    override fun load() {

        interactor.getWordsToLearn().bind { words: List<Word> ->
            wordsSubj eats words
        }
    }

    override fun wordPressed(word: Word) {
        if (!chosenWordsCache.remove(word)) {
            chosenWordsCache.add(word)
            autoSlide eats Unit
        }
        wordsAmountSubj eats chosenWordsCache.size

        if (chosenWordsCache.size > 7) {
            shouldGoNext eats Unit
            end()
        }

        interactor.addWordsForLearning(chosenWordsCache)
    }

    override fun words(): Observable<List<Word>> = wordsSubj
    override fun autoSlide(): Observable<Unit> = autoSlide

    override fun isWordChosen(word: Word): Boolean = chosenWordsCache.contains(word)
    override fun getWordsAmount() = wordsAmountSubj

    override fun end() {
        super.end()
        interactor.addWordsForLearning(chosenWordsCache)
        chosenWordsCache.clear()
    }

}