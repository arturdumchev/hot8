package com.livermor.hot8app.feature.repetition

import com.livermor.hot8app.common.model.LearningProgress
import java.util.*


class WordsToRepeatNow {
    var list: List<LearningProgress> = Collections.emptyList()
}