//https://medium.com/@ipaulpro/drag-and-swipe-with-recyclerview-6a6f0c422efd#.y61jvv7d6
package com.livermor.hot8app.feature.common.view.rv.drag_and_drop

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log


class DragItemTouchHelperCallback(private val mAdapter: DragAndSwipeAdapter) : ItemTouchHelper.Callback() {

    companion object {
        private const val TAG = "SimpleItemTouchHelp"
        private const val ALPHA_FULL = 1.0f
    }

    override fun isLongPressDragEnabled(): Boolean = false
    override fun isItemViewSwipeEnabled(): Boolean = true

    override fun getMovementFlags(recyclerView: RecyclerView,
                                  viewHolder: RecyclerView.ViewHolder): Int {

        val dragFlags = 0 //ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        val swipeFlags = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView,
                        source: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder): Boolean {

        if (source.itemViewType != target.itemViewType) return false
        if (source.adapterPosition >= 0 && target.adapterPosition >= 0) {
            mAdapter.onItemMove(source.adapterPosition, target.adapterPosition)
        }
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
        mAdapter.onItemDismiss(viewHolder.adapterPosition)

        if (viewHolder is ItemSelectedListener) {
            viewHolder.onItemRemoved()
        }
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        Log.i(TAG, "actionState == $actionState")
        if (viewHolder is ItemSelectedListener) when (actionState) {
            ItemTouchHelper.ACTION_STATE_DRAG -> {
                Log.i(TAG, "ACTION_STATE_DRAG")
                viewHolder.onItemSelected()
            }
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        viewHolder.itemView.alpha = ALPHA_FULL

        if (viewHolder is ItemSelectedListener) {
            viewHolder.onItemClear()
        }
    }

    //——————————————————————————————————————————————————————————————————————

    // more swipe — more invisibility
    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder,
                             dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            val width = viewHolder.itemView.width;
            val alpha = 1.0f - Math.abs(dX) / width;
            viewHolder.itemView.alpha = alpha;
            viewHolder.itemView.translationX = dX;
        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY,
                    actionState, isCurrentlyActive);
        }
    }

    // to draw red behind
/*    override fun onChildDraw(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        ItemTouchHelper.Callback.getDefaultUIUtil().onDraw(c, recyclerView, (viewHolder as ItemSelectedListener)
                .getSwipeView(), dX, dY, actionState, isCurrentlyActive)
    }

    override fun onChildDrawOver(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        ItemTouchHelper.Callback.getDefaultUIUtil().onDrawOver(c, recyclerView, (viewHolder as ItemSelectedListener)
                .getSwipeView(), dX, dY, actionState, isCurrentlyActive)
    }*/
}