package com.livermor.hot8app.feature.common.injection.feature

import android.content.Context
import com.livermor.hot8app.AppComponent
import com.livermor.hot8app.data.preference.global.IGlobalData
import com.livermor.hot8app.common.error.ErrorHandlerModule
import com.livermor.hot8app.common.error.IErrorHandler
import dagger.Component


@Feature
@Component(
        dependencies = arrayOf(AppComponent::class),
        modules = arrayOf(ErrorHandlerModule::class))
interface FeatureComponent {
    fun provideErrorHandler(): IErrorHandler
    fun getGlobalData(): IGlobalData
    fun provideContext(): Context
}