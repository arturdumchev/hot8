package com.livermor.hot8app.feature.common.view.view_pager

import android.support.v4.view.ViewPager
import android.view.View


class MyPagerTransformer : ViewPager.PageTransformer {

    private val SCALE_FACTOR_SLIDE = 0.85f
    private val MIN_ALPHA_SLIDE = 0.35f

    override fun transformPage(page: View, position: Float) {

        val alpha: Float
        val scale: Float
        val translationX: Float

        if (position < 0 && position > -1) {
            // this is the page to the left
            scale = Math.abs(Math.abs(position) - 1) * (1.0f - SCALE_FACTOR_SLIDE) + SCALE_FACTOR_SLIDE
            alpha = Math.max(MIN_ALPHA_SLIDE, 1 - Math.abs(position))
            val pageWidth = page.width
            val translateValue = position * - pageWidth
            if (translateValue > -pageWidth) {
                translationX = translateValue
            } else {
                translationX = 0f
            }
        } else {
            alpha = 1f
            scale = 1f
            translationX = 0f
        }

        page.alpha = alpha
        page.translationX = translationX
        page.scaleX = scale
        page.scaleY = scale
    }
}