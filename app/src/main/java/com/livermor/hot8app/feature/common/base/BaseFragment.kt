package com.livermor.hot8app.feature.common.base

import android.support.v4.app.Fragment
import android.view.View
import com.livermor.hot8app.common.NOSYNC
import com.livermor.hot8app.common.util.addTo
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {

    protected val disposables by lazy(NOSYNC) { CompositeDisposable() }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }

    inline protected fun <reified T : View> inflate(res: Int): T {
        return activity.layoutInflater.inflate(res, null) as T
    }

    //—————————————————————————————————————————————————————————————————————— rx

    protected fun <T> Observable<T>.bindUi(action: (T) -> Unit) {
        compose(onUiCachedScheduler())
                .subscribe { t -> action.invoke(t) }
                .addTo(disposables)
    }

    //—————————————————————————————————————————————————————————————————————— rx helpers

    private val schedulersTransformer = ObservableTransformer<Any, Any> { upstream ->
        upstream.observeOn(AndroidSchedulers.mainThread())
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> onUiCachedScheduler() = schedulersTransformer as ObservableTransformer<T, T>
}