package com.livermor.hot8app.feature.common.base

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import com.livermor.hot8app.common.NOSYNC
import com.livermor.hot8app.common.util.addTo
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


abstract class BaseActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "BaseActivity"

    }


    abstract protected val viewModel: IBaseVM
    abstract protected val activityRoot: ViewGroup

    protected val disposables by lazy(NOSYNC) { CompositeDisposable() }


    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
        viewModel.end()
    }

    //—————————————————————————————————————————————————————————————————————— rx helpers
    fun <T> Observable<T>.bindUi(action: (T) -> Unit) {
        compose(onUiCachedScheduler())
                .subscribe { t -> action.invoke(t) }
                .addTo(disposables)
    }

    //——————————————————————————————————————————————————————————————————————

    private val schedulersTransformer = ObservableTransformer<Any, Any> { upstream ->
        upstream.observeOn(AndroidSchedulers.mainThread())
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> onUiCachedScheduler() = schedulersTransformer as ObservableTransformer<T, T>

    fun Class<*>.goTo() = with(this@BaseActivity) {
        startActivity(Intent(this, this@goTo))
    }
}