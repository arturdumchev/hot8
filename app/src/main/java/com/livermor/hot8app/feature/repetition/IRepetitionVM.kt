package com.livermor.hot8app.feature.repetition

import com.livermor.hot8app.feature.learning.train_fragments.communication.ITransHelper
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import io.reactivex.Observable


interface IRepetitionVM : ITransHelper {

    fun getTransDataObservable(): Observable<List<TransData>>
    fun goNext(): Observable<Unit>
}