package com.livermor.hot8app.feature.choose_words

import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.feature.common.base.IBaseVM
import io.reactivex.Observable


interface IChooseWordsVM : IBaseVM {
    fun load()
    fun words(): Observable<List<Word>>
    fun autoSlide(): Observable<Unit>

    fun isWordChosen(word: Word): Boolean
    fun wordPressed(word: Word)
    fun getWordsAmount(): Observable<Int>
}