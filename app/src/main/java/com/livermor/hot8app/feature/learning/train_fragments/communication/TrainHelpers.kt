package com.livermor.hot8app.feature.learning.train_fragments.communication

import io.reactivex.Observable


interface ITrainHelper

interface ITransHelper : ITrainHelper {

    fun chooseWord(word: String,
                   rightWord: String,
                   isFromEng: Boolean,
                   fragPos: Int): Boolean

}

interface IWritingHelper : ITrainHelper {

    fun isRightLetter(word: String, wordObservable: Observable<String>): Observable<Boolean>

    fun isRightLetter(char: Char, writtenText: String, actualText: String): Boolean

    fun writtenWordConfirmed(userWord: String,
                             realWord: String,
                             fragPos: Int): Boolean

    fun changeProficientMode()
}
