package com.livermor.hot8app.feature.common

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

fun EditText.observableFromEditText(): Observable<String> {
    val subj = BehaviorSubject.create<String>()
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
            subj.onNext(p0.toString())
        }
    })
    return subj
}