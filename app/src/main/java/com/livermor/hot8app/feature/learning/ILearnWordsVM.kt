package com.livermor.hot8app.feature.learning


import com.livermor.hot8app.feature.common.base.IBaseVM
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITransHelper
import com.livermor.hot8app.feature.learning.train_fragments.communication.IWritingHelper
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import io.reactivex.Observable


interface ILearnWordsVM : IBaseVM, IWritingHelper, ITransHelper {

    fun getTrainData(): Observable<List<TrainData>>
    fun scrollToPosition(): Observable<Int>
    fun goNext(): Observable<Unit>
}
