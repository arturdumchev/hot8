package com.livermor.hot8app.feature.choose_words

import android.os.Bundle
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import com.livermor.hot8app.R
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.feature.common.base.BaseFragmentParent
import com.livermor.hot8app.feature.common.injection.learner.LearnerComponentHolder
import com.livermor.hot8app.feature.common.view.view_pager.MyPagerTransformer
import kotlinx.android.synthetic.main.choose_words_activity.*
import javax.inject.Inject

class ChooseWordsFrag : BaseFragmentParent(), ChooseWordsChildFrag.Callback {

    companion object {
        private const val TAG = "ChooseWordsActivity"
    }

    @Inject lateinit override var viewModel: ChooseWordsVM
    override val fragmentParentId: Int get() = R.id.activity_choose_words
    override val fragmentRootLayoutId: Int get() = R.layout.choose_words_activity

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        LearnerComponentHolder.component.inject(this)
        setUpVM()
    }

    private fun setUpVM() = with(viewModel) {
        load()
        words().bindUi { words: List<Word> -> setUpViewPager(words) }
        //goNext().bindUi { startActivity(Intent(this@ChooseWordsActivity, LearnWordsActivity::class.java)) }
    }

    private fun setUpViewPager(words: List<Word>) {
        choose_words_view_pager.adapter = createFragmentPagerAdapter(words)
        //choose_words_view_pager.setPageTransformer(false, MyPagerTransformer())
    }

    private fun createFragmentPagerAdapter(words: List<Word>): FragmentPagerAdapter {
        return object : FragmentPagerAdapter(childFragmentManager) {
            override fun getItem(position: Int) = ChooseWordsChildFrag.instance(words[position])
            override fun getCount(): Int = words.size
        }
    }

    //—————————————————————————————————————————————————————————————————————— frag callback

    override fun getChosenWordsCount() = viewModel.getWordsAmount()

    override fun wordButtonPressed(word: Word) {
        viewModel.wordPressed(word)
        choose_words_view_pager.apply {
            currentItem += 1
        }
    }

    override fun isWordEnabled(word: Word): Boolean = viewModel.isWordChosen(word)
}
