package com.livermor.hot8app.feature.common.view.rv

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup


interface IDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(
            holder: RecyclerView.ViewHolder,
            item: IDelegateViewType)
}

interface IDelegateViewType {
    fun getViewType(): Int
}


//——————————————————————————————————————————————————————————————————————

object DelegateViewTypeId{

}



