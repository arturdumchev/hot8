package com.livermor.hot8app.feature.learning.train_fragments.frags

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.clearFromChildrenIfAny
import com.livermor.hot8app.feature.common.inflate
import com.livermor.hot8app.feature.common.linearLP
import com.livermor.hot8app.feature.common.linearMP
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITransHelper
import kotlinx.android.synthetic.main.learn_translation_frag.*
import java.util.*


open class TransFragment : BaseTrainFrag<ITransHelper>() {
    companion object {
        private val TAG = TransFragment::class.java.simpleName

        private const val ARG_WORD = "ARG_WORD"
        private const val ARG_TRANS = "ARG_TRANS"
        private const val ARG_FROM_ENG = "ARG_FROM_ENG"
        private const val ARG_TRANS_VARS = "ARG_TRANS_VARS"
        private const val ARG_POS = "ARG_POS"

        fun instance(word: String,
                     trans: String,
                     fromEng: Boolean,
                     translations: List<String>,
                     pos: Int) = TransFragment().apply {

            arguments = Bundle().apply {
                putString(ARG_WORD, word)
                putString(ARG_TRANS, trans)
                putBoolean(ARG_FROM_ENG, fromEng)
                putStringArrayList(ARG_TRANS_VARS, ArrayList(translations))
                putInt(ARG_POS, pos)
            }

        }
    }

    open protected val pos: Int by lazy(N) { arguments.getInt(ARG_POS) }

    open protected val mainWord: String by lazy(N) { arguments.getString(ARG_WORD) }
    open protected val mainTrans: String by lazy(N) { arguments.getString(ARG_TRANS) }
    open protected val isFromEng: Boolean by lazy(N) { arguments.getBoolean(ARG_FROM_ENG) }
    open protected val translations: List<String> by lazy(N) { arguments.getStringArrayList(ARG_TRANS_VARS) }

    open protected val textViews by lazy(N) { ArrayList<TextView>() }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.learn_translation_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpLogic()
    }

    open protected fun setUpLogic() = with(helper) {
        tran_texts_linear_container.clearFromChildrenIfAny()
        trans_word_text.text = mainWord
        setUpTranslations()
    }

    private fun ITransHelper.setUpTranslations() {
        textViews.clear()
        translations.forEach { translation ->
            val transText = inflate<TextView>(R.layout.custom_text_view_template).apply {
                text = translation
                textViews.add(this)
                setOnClickListener {
                    chooseWord(translation, mainTrans, isFromEng, pos)
                    onTransChosen()
                }
            }
            tran_texts_linear_container.addView(transText, linearLP(linearMP(), 0, 1f))
        }
    }

    open protected fun TextView.onTransChosen() {
        forbidClicks()

        if (text.toString().isRight()) markSuccess(true)
        else {
            markSuccess(false)
            getRightTextView().markSuccess(true)
        }
    }

    open protected fun String.isRight() = this == mainTrans
    open protected fun getRightTextView() = textViews.filter { it.text.toString() == mainTrans }.first()
    open protected fun forbidClicks() = textViews.forEach { it.isClickable = false }

    open protected fun TextView.markSuccess(isSuccess: Boolean) {
        val color = if (isSuccess) Color.GREEN else Color.RED
        this.setBackgroundColor(color)
    }
}