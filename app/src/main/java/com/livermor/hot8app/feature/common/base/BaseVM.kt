package com.livermor.hot8app.feature.common.base

import com.livermor.hot8app.common.error.CommonError
import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.NOSYNC
import com.livermor.hot8app.common.util.addTo
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable


abstract class BaseVM : IBaseVM {

    private val TAG = BaseVM::class.java.simpleName

    protected val disposables by lazy(NOSYNC) { CompositeDisposable() }
    protected abstract val errorHandler: IErrorHandler

    override fun errorEmitter(): Observable<String> = errorHandler.msgObservable()
    override fun end() {
        disposables.clear()
    }

    protected fun error(commonError: CommonError) {
        errorHandler.error(commonError)
    }

    protected fun error(throwable: Throwable) {
        errorHandler.error(throwable)
    }

    protected inline fun <T> Single<T>.bind(crossinline onNext: (T) -> Unit) {
        subscribe({ onNext.invoke(it) }, { error(it) }).addTo(disposables)
    }

    protected inline fun <T> Observable<T>.bind(crossinline onNext: (T) -> Unit) = bind(onNext, { error(it) })
    protected inline fun <T> Completable.bind(crossinline onNext: () -> Unit) = bind(onNext, { error(it) })

    protected inline fun <T> Observable<T>.bind(crossinline onNext: (T) -> Unit,
                                                crossinline onError: (Throwable) -> Unit) {
        subscribe({ onNext.invoke(it) }, { onError.invoke(it) }).addTo(disposables)
    }

    protected inline fun Completable.bind(crossinline onComplete: () -> Unit,
                                          crossinline onError: (Throwable) -> Unit) {
        subscribe({ onComplete.invoke() }, { onError.invoke(it) }).addTo(disposables)
    }
}