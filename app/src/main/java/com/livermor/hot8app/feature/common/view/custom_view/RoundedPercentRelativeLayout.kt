package com.livermor.hot8app.feature.common.view.custom_view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.support.percent.PercentRelativeLayout
import android.util.AttributeSet
import com.livermor.hot8app.R
import kotlin.properties.Delegates


class RoundedPercentRelativeLayout : PercentRelativeLayout {

    companion object {
        private const val TAG = "WindowRelativeLayout"
        private const val NON_TITLE_RES = -1
    }

    private val path by lazy { Path() }
    private val rect by lazy { RectF() }
    private var radius: Float by Delegates.notNull()

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val attributeValuesStyle = getContext().theme
                .obtainStyledAttributes(attrs, R.styleable.RoundedRelativeLayout, 0, 0)
        try {
            radius = attributeValuesStyle.getDimension(R.styleable.RoundedRelativeLayout_corner, 0f)
        } finally {
            attributeValuesStyle.recycle()
        }

        init()
    }

    private fun init() {
        //val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //val view = inflater.inflate(R.layout.custom_window_layout, this, true)
    }

    override fun onSizeChanged(w: Int, h: Int, oldW: Int, oldH: Int) {
        super.onSizeChanged(w, h, oldW, oldH)

        path.reset()
        rect.set(0f, 0f, w.toFloat(), h.toFloat())
        path.addRoundRect(rect, radius, radius, Path.Direction.CW)
        path.close()
    }

    override fun dispatchDraw(canvas: Canvas) {
        val save = canvas.save()
        canvas.clipPath(path)
        super.dispatchDraw(canvas)
        canvas.restoreToCount(save)
    }
}