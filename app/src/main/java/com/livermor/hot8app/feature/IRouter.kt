package com.livermor.hot8app.feature


interface IRouter {

    fun showMainFrag()

    fun showTraining()

    fun showRepetition()

    fun showChooseWords()
}