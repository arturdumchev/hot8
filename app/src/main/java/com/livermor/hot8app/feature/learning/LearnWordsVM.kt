package com.livermor.hot8app.feature.learning

import android.util.Log
import com.livermor.hot8app.common.N
import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.util.bs
import com.livermor.hot8app.common.util.eats
import com.livermor.hot8app.common.util.ps
import com.livermor.hot8app.domain.learn_words.ILearnInteractor
import com.livermor.hot8app.domain.learn_words.Training
import com.livermor.hot8app.feature.common.base.BaseVM
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import com.livermor.hot8app.feature.repetition.WordsToRepeatCache
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject


class LearnWordsVM @Inject constructor(
        override var errorHandler: IErrorHandler,
        private val learnInteractor: ILearnInteractor) : ILearnWordsVM, BaseVM() {

    companion object {
        private const val TAG = "LearnWordsVM"
    }

    private val trainingToWordSubj by lazy(N) { bs<List<TrainData>>() }
    private var currenWordHadMistake = false
    private val positionSubj by lazy(N) { ps<Int>() }
    private val goNextSubj by lazy(N) { ps<Unit>() }

    private var currentRound = 0
    private var wordPacksLearned = 0

    override fun getTrainData(): BehaviorSubject<List<TrainData>> {
        requestForTrainData(Collections.emptyList())
        return trainingToWordSubj
    }

    override fun scrollToPosition(): Observable<Int> = positionSubj
    override fun goNext(): Observable<Unit> = goNextSubj

    //—————————————————————————————————————————————————————————————————————— frag helper


    override fun isRightLetter(word: String, wordObservable: Observable<String>): Observable<Boolean> {
        return wordObservable.map { writing ->
            val right = word.contains(writing, true)
            if (!right) currenWordHadMistake = true
            right
        }
    }

    override fun isRightLetter(char: Char, writtenText: String, actualText: String): Boolean {
        val right = actualText.contains(writtenText + char, true)
        if (!right) currenWordHadMistake = true
        return right
    }

    override fun writtenWordConfirmed(userWord: String, realWord: String, fragPos: Int): Boolean {
        val wasRight = (userWord == realWord)
        doOnAnswer(fragPos, wasRight && !currenWordHadMistake)
        currenWordHadMistake = false

        return wasRight
    }

    override fun chooseWord(word: String,
                            rightWord: String,
                            isFromEng: Boolean,
                            fragPos: Int): Boolean {
        val wasRight = word == rightWord
        doOnAnswer(fragPos, wasRight)
        return wasRight
    }

    override fun changeProficientMode() {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //—————————————————————————————————————————————————————————————————————— helpers

    private fun doOnAnswer(fragPos: Int, wasRight: Boolean) {
        (trainData() [fragPos] as ProgressData).let {
            if (wasRight) it.success = true
            it.attempt++
        }

        if (isEndOfRound(fragPos)) updateOnLastElement()
        positionSubj eats nextPosition(fragPos)
    }

    private fun updateOnLastElement() {
        if (learnInteractor.haveDoneEnoughRounds(++currentRound)) {
            Log.i(TAG, "doOnAnswer: AttemptsWasted")
            doOnRoundEnd()
        } else {
            requestForTrainData(trainData())
        }
    }

    private fun doOnRoundEnd() {
        currentRound = 0
        wordPacksLearned++
        if (learnInteractor.haveLearnedEnoughPacks(wordPacksLearned)) {
            Log.e(TAG, "doOnRoundEnd: THE END!")
            saveWordsForRepetition()
            goNextSubj eats Unit

        } else {
            learnInteractor.saveTrainedData(trainData())
            requestForTrainData(Collections.emptyList())
        }
    }

    private fun saveWordsForRepetition() {
        val repeatData = ArrayList<TransData>()
        trainData().forEach { data ->
            if (data is TransData && data.training == Training.TRANS_FROM_ENG) {
                repeatData.add(data)
            }
        }
        WordsToRepeatCache.list = repeatData
    }

    private fun requestForTrainData(data: List<TrainData>) {
        learnInteractor.getTrainData(data).bind { newData -> trainingToWordSubj eats newData }
    }

    private fun isEndOfRound(fragPos: Int) = fragPos == trainData().size - 1
    private fun trainData(): List<TrainData> = trainingToWordSubj.value
    private fun nextPosition(lastPos: Int): Int {

        val nextPos: Int? = trainData().withIndex()
                .drop(lastPos + 1)
                .firstOrNull { it.value.shouldContinue }
                ?.index

        return when {
            lastPos > trainData().size -> 0      //throw IllegalArgumentException("last trainData's position can't be bigger, than size of trainData")
            lastPos == 0 && nextPos == null -> 0 // no items remain
            nextPos == null -> nextPosition(0)   // end of the list, should start from start
            else -> nextPos                      // norm next pos
        }
    }
}