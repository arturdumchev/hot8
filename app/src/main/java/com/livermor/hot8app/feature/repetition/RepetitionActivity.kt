package com.livermor.hot8app.feature.repetition

import android.os.Bundle
import android.view.ViewGroup
import com.livermor.hot8app.R
import com.livermor.hot8app.feature.common.base.BaseActivity
import com.livermor.hot8app.feature.common.injection.learner.LearnerComponentHolder
import com.livermor.hot8app.feature.learning.train_fragments.frags.BaseTrainFrag
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITransHelper
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import kotlinx.android.synthetic.main.repetition_activity.*
import javax.inject.Inject

class RepetitionActivity : BaseActivity(), BaseTrainFrag.Callback<ITransHelper> {

    companion object {
        private const val TAG = "RepetitionActivity"
    }

    override val activityRoot: ViewGroup get() = activity_repetition
    @Inject override lateinit var viewModel: RepetitionVM

    override fun onCreate(savedInstanceState: Bundle?) {
        LearnerComponentHolder.component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.repetition_activity)
        setUpVM()
    }

    private fun setUpVM() = with(viewModel) {
        getTransDataObservable().bindUi { words -> setUpAdapter(words) }
        goNext().bindUi { repetition_view_pager.currentItem += 1 }
    }

    private fun setUpAdapter(wordsToRepeat: List<TransData>) {
        val adapter = RepetitionAdapter(supportFragmentManager, wordsToRepeat)
        repetition_view_pager.adapter = adapter
        repetition_view_pager.setPagingEnabled(false)
    }

    //——————————————————————————————————————————————————————————————————————
    override fun getHelper(): ITransHelper = viewModel
}
