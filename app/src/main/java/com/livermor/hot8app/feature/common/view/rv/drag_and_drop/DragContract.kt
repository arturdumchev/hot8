package com.livermor.hot8app.feature.common.view.rv.drag_and_drop

import android.support.v7.widget.RecyclerView
import android.view.View


interface DragAndSwipeAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int)
    fun onItemDismiss(position: Int)
}

// implemented by ViewHolders
interface ItemSelectedListener {
    fun onItemSelected()
    fun onItemClear()
    fun getSwipeView(): View  // return a ViewGroup that could be swiped
    fun onItemRemoved()
}

// easy implement in ItemTouchHelper.Callback
interface DragEventsListener {
    fun onDragStarted(viewHolder: RecyclerView.ViewHolder)
    fun onDragTo(viewHolder: RecyclerView.ViewHolder, isActive: Boolean)
    fun onDragReleased(viewHolder: RecyclerView.ViewHolder?)
}

interface DraggedItem