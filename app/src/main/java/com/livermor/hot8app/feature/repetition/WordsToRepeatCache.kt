package com.livermor.hot8app.feature.repetition

import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import java.util.*


// using just a singleton, cause we don't have to cache it, if app died
object WordsToRepeatCache {
    var list: List<TransData> = Collections.emptyList()
}