package com.livermor.hot8app.feature.common

import android.content.Context
import android.graphics.Rect
import android.support.v4.view.ViewPager
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout


//—————————————————————————————————————————————————————————————————————— VIEW_COMMON
fun View.rect(): Rect {
    val rect = Rect()
    getHitRect(rect)
    return rect
}

fun View.visibleNotGone(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.visibleNotInvisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.INVISIBLE
}

fun View.showView(show: Boolean, viewRoot: ViewGroup) {
    TransitionManager.beginDelayedTransition(viewRoot)
    visibleNotGone(show)
}

//—————————————————————————————————————————————————————————————————————— VIEW_GROUP

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View
        = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun <T : View> ViewGroup.onEachChild(func: (T) -> Unit) {
    for (i in 0..this.childCount - 1) {
        @Suppress("UNCHECKED_CAST") func.invoke(this.getChildAt(i) as T)
    }
}

fun ViewGroup.clearFromChildrenIfAny() {
    if (childCount > 0) removeAllViews()
}

//—————————————————————————————————————————————————————————————————————— GLOBAL

fun Context.dencity() = resources.displayMetrics.density

fun View.statusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}


fun Context.getDimen(dimenRes: Int): Int {
    return resources.getDimensionPixelOffset(dimenRes)
}
//—————————————————————————————————————————————————————————————————————— LAYOUT PARAMS
fun relativeLP(width: Number, height: Number): RelativeLayout.LayoutParams =
        RelativeLayout.LayoutParams(width.toInt(), height.toInt())

fun linearMP() = LinearLayout.LayoutParams.MATCH_PARENT

fun linearLP(width: Number, height: Number): LinearLayout.LayoutParams =
        LinearLayout.LayoutParams(width.toInt(), height.toInt())

fun linearLP(width: Number, height: Number, weight: Float): LinearLayout.LayoutParams {
    val lp = LinearLayout.LayoutParams(width.toInt(), height.toInt())
    lp.weight = weight
    return lp
}

//—————————————————————————————————————————————————————————————————————— VIEW_PAGER

fun ViewPager.next() {
    currentItem += 1
}
