package com.livermor.hot8app.feature.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livermor.hot8app.feature.common.inflate


abstract class BaseFragmentParent : BaseFragment() {

    companion object {
        private val TAG = BaseFragmentParent::class.java.simpleName
    }

    abstract protected val viewModel: IBaseVM
    abstract protected val fragmentParentId: Int
    abstract protected val fragmentRootLayoutId: Int

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?): View?
            = v?.inflate(fragmentRootLayoutId)

}