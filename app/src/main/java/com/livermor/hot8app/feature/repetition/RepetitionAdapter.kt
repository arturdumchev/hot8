package com.livermor.hot8app.feature.repetition

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainAdapterTrait
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData


class RepetitionAdapter(fm: FragmentManager, private val wordsToRepeat: List<TransData>) :
        FragmentStatePagerAdapter(fm), TrainAdapterTrait {

    override fun getItem(position: Int): Fragment = with(wordsToRepeat[position]) {
        return RepetitionFragment.instance(word(),
                trans(),
                true,
                getTranslations(this.wordProgress.word, translations),
                position)
    }

    override fun getCount(): Int = wordsToRepeat.size

    private fun getTranslations(word: Word, translations: List<String>): List<String> {
        return listOf(word.translation, translations.filter { it != word.translation }.first())
    }
}