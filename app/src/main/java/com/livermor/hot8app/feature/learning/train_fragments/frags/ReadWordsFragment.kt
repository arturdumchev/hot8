package com.livermor.hot8app.feature.learning.train_fragments.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.common.util.MapArrayConverter
import com.livermor.hot8app.feature.common.inflate
import com.livermor.hot8app.feature.common.linearLP
import com.livermor.hot8app.feature.common.view.custom_view.WordCustomRelLayout
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITrainHelper
import kotlinx.android.synthetic.main.learn_read_frag.*


class ReadWordsFragment : BaseTrainFrag<ITrainHelper>() {

    companion object {
        private val TAG = ReadWordsFragment::class.java.simpleName
        private const val ARG_WORD_TO_TRANS = "ARG_WORD_TO_TRANS"
        fun instance(wordToTrans: Map<String, String>) = ReadWordsFragment().apply {
            arguments = Bundle().apply {
                putStringArray(ARG_WORD_TO_TRANS, MapArrayConverter.getArr(wordToTrans))
            }
        }


    }

    private val wordToTrans: Map<String, String> by lazy(N) {
        MapArrayConverter.getMap(arguments.getStringArray(ARG_WORD_TO_TRANS))
    }

/*    private val wordsTexts by lazy(NOSYNC) {
        listOf(learn_read_first, learn_read_second, learn_read_third, learn_read_forth)
    }*/
    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.learn_read_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    //——————————————————————————————————————————————————————————————————————

    private fun setUpView() {

        for ((word, translation) in wordToTrans) {
            val wordPair = WordCustomRelLayout(context)
            val lp = linearLP(LinearLayout.LayoutParams.MATCH_PARENT, 0)
            lp.weight = 1f
            read_words_linear_container.addView(wordPair, lp)

            wordPair.setText1(word)
            wordPair.setText2(translation)
        }
    }
}