package com.livermor.hot8app.feature.learning.train_fragments.communication


interface TrainAdapterTrait {
    fun ProgressData.word() = wordProgress.word.writing
    fun ProgressData.trans() = wordProgress.word.translation
}