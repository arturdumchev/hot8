package com.livermor.hot8app.feature.choose_words

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livermor.hot8app.R
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.feature.common.base.BaseFragment
import com.livermor.hot8app.feature.common.inflate
import io.reactivex.Observable
import kotlinx.android.synthetic.main.choose_words_frag.*

class ChooseWordsChildFrag : BaseFragment() {

    companion object {
        private const val TAG = "ChooseWordsFrag"

        fun instance(word: Word): ChooseWordsChildFrag {
            val frag = ChooseWordsChildFrag()
            frag.word = word
            return frag
        }
    }

    private lateinit var word: Word
    private var callback: Callback? = null

    interface Callback {
        fun getChosenWordsCount(): Observable<Int>
        fun wordButtonPressed(word: Word)
        fun isWordEnabled(word: Word): Boolean
    }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?): View?
            = v?.inflate(R.layout.choose_words_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() = with(callback!!) {

        getChosenWordsCount().bindUi { count: Int ->
            choose_words_chosen_amount.text = count.toString()

            if (isWordEnabled(word)) {
                choose_words_choose_but.text = "СЛОВО ВЫБРАНО"
            } else {
                choose_words_choose_but.text = "ВЫБРАТЬ СЛОВО"
            }
        }

        choose_words_word.text = word.writing
        choose_words_translation.text = word.translation
        choose_words_choose_but.setOnClickListener {
            wordButtonPressed(word)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = parentFragment as ChooseWordsFrag
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }
}