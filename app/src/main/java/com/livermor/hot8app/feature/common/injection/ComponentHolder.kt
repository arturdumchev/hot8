package com.livermor.hot8app.feature.common.injection


abstract class ComponentHolder<out T> {
    private var currentComponent: T? = null

    val component: T get() {
        if (currentComponent == null) {
            currentComponent = createComponent()
        }
        return currentComponent!!
    }

    abstract fun createComponent(): T

    fun clear() {
        currentComponent = null
    }
}