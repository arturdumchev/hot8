package com.livermor.hot8app.feature.common.fragment

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.livermor.hot8app.R

fun FragmentManager.addFrag(containerId: Int, frag: Fragment) {
    beginTransaction()
            .setCustomAnimations(R.animator.start_form_right_to_left, R.animator.end_alpha,
                    R.animator.start_alpha, R.animator.end_from_left_to_right)
            .replace(containerId, frag)
            .addToBackStack(null)
            .commit()
}