package com.livermor.hot8app.feature.repetition

import android.util.Log
import com.livermor.hot8app.common.N
import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.util.bs
import com.livermor.hot8app.common.util.eats
import com.livermor.hot8app.common.util.ps
import com.livermor.hot8app.domain.learn_words.ILearnInteractor
import com.livermor.hot8app.feature.common.base.BaseVM
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import io.reactivex.Observable
import javax.inject.Inject


class RepetitionVM @Inject constructor(
        override val errorHandler: IErrorHandler,
        private val learnInteractor: ILearnInteractor) : BaseVM(), IRepetitionVM {

    companion object {
        private val TAG = RepetitionVM::class.java.simpleName
    }

    private val transDataSubj by lazy(N) { bs<List<TransData>>() }
    private val positionSubj by lazy(N) { ps<Unit>() }

    override fun getTransDataObservable(): Observable<List<TransData>> {
        if (WordsToRepeatCache.list.isNotEmpty()) {
            transDataSubj eats WordsToRepeatCache.list
        }
        return transDataSubj
    }

    override fun goNext(): Observable<Unit> = positionSubj
    override fun chooseWord(word: String,
                            rightWord: String,
                            isFromEng: Boolean,
                            fragPos: Int): Boolean {
        val wasRight = word == rightWord
        doOnAnswer(fragPos, wasRight)
        return wasRight
    }

    private fun doOnAnswer(pos: Int, wasRight: Boolean) {
        transData()[pos].success = wasRight
        when {
            isEndOfRound(pos) -> doOnEnd()
            else -> positionSubj eats Unit
        }
    }

    private fun doOnEnd() {
        learnInteractor.saveTrainedData(transData())
        Log.e(TAG, "doOnEnd: just saved repetition results")
    }

    private fun transData(): List<TransData> = transDataSubj.value
    private fun isEndOfRound(fragPos: Int) = fragPos == transData().size - 1
}

