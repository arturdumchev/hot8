package com.livermor.hot8app.feature.common.injection.learner

import com.livermor.hot8app.data.repo.LearnerDataModule
import com.livermor.hot8app.domain.LearnerDomainModule
import com.livermor.hot8app.feature.choose_words.ChooseWordsFrag
import com.livermor.hot8app.feature.common.injection.feature.FeatureComponent
import com.livermor.hot8app.feature.learning.LearnWordsActivity
import com.livermor.hot8app.feature.repetition.RepetitionActivity
import dagger.Component


@Learner
@Component(
        modules = arrayOf(LearnerDataModule::class, LearnerDomainModule::class),
        dependencies = arrayOf(FeatureComponent::class))
interface LearnerComponent {
    fun inject(chooseWordsActivity: ChooseWordsFrag)
    fun inject(learnWordsActivity: LearnWordsActivity)
    fun inject(repetitionActivity: RepetitionActivity)
}