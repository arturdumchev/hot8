package com.livermor.hot8app.feature.common.injection.feature

import com.livermor.hot8app.App
import com.livermor.hot8app.feature.common.injection.ComponentHolder


object FeatureComponentHolder : ComponentHolder<FeatureComponent>() {

    override fun createComponent(): FeatureComponent =
            DaggerFeatureComponent.builder()
                    .appComponent(App.appComponent)
                    .build()
}