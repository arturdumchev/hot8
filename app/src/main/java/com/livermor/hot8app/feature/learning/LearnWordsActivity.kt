package com.livermor.hot8app.feature.learning

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.ViewGroup
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.base.BaseActivity
import com.livermor.hot8app.feature.common.injection.learner.LearnerComponentHolder
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITrainHelper
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.frags.BaseTrainFrag
import com.livermor.hot8app.feature.repetition.RepetitionActivity
import kotlinx.android.synthetic.main.learn_words_activity.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class LearnWordsActivity : BaseActivity(), BaseTrainFrag.Callback<ITrainHelper> {

    companion object {
        private const val TAG = "LearnWordsActivity"
        const val CHANGE_PAGE_DELAY = 100L
    }

    @Inject override lateinit var viewModel: LearnWordsVM
    override val activityRoot: ViewGroup get() = learn_words_root
    private val adapter by lazy(N) { LearnWordsPagerAdapter(supportFragmentManager) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LearnerComponentHolder.component.inject(this)
        setContentView(R.layout.learn_words_activity)

        setUpViews()
        setUpVM()
    }

    private fun setUpVM() = with(viewModel) {
        scrollToPosition().delay(CHANGE_PAGE_DELAY, TimeUnit.MILLISECONDS).bindUi {
            learn_words_view_pager.currentItem = it
        }

        getTrainData().bindUi { trainData ->
            trainData.forEach { Log.i(TAG, "setUpViews: ${it.training} ${(it as? ProgressData)?.wordProgress?.word?.writing ?: ""}"); }
            adapter.trainData = trainData
            adapter.notifyDataSetChanged()

            learn_words_view_pager.setCurrentItem(0, false)
        }

        goNext().bindUi { RepetitionActivity::class.java.goTo() }
    }

    private fun setUpViews() = with(viewModel) {
        setUpAdapter()
    }

    private fun setUpAdapter() {
        learn_words_view_pager.adapter = adapter
        learn_words_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                Log.d(TAG, "onPageScrollStateChanged: state == $state")
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Log.d(TAG, "onPageScrolled: position == $position")
            }

            override fun onPageSelected(position: Int) {
                Log.d(TAG, "onPageSelected: position == $position")
                val setPagingEnabled = position == 0
                Log.i(TAG, "onPageSelected: about to set setPagingEnabled to $setPagingEnabled")
                learn_words_view_pager.setPagingEnabled(setPagingEnabled)
            }

        })
    }

    //—————————————————————————————————————————————————————————————————————— trainings callback

    override fun getHelper(): ITrainHelper = viewModel
}
