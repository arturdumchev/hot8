package com.livermor.hot8app.feature.common.view.rv.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;


public class EverySideSpaceItemDecor extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeight;

    public EverySideSpaceItemDecor(int mVerticalSpaceHeight) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = mVerticalSpaceHeight;
        outRect.right = mVerticalSpaceHeight;
    }
}
