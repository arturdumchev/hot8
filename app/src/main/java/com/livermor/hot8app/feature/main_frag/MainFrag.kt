package com.livermor.hot8app.feature.main_frag

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livermor.hot8app.R
import com.livermor.hot8app.feature.common.base.BaseFragment
import com.livermor.hot8app.feature.common.inflate


class MainFrag : BaseFragment() {
    companion object {
        private val TAG = MainFrag::class.java.simpleName
    }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?): View?
            = v?.inflate(R.layout.main_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}