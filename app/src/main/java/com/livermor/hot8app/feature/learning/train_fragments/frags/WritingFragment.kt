package com.livermor.hot8app.feature.learning.train_fragments.frags

import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.GridLayout
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.*
import com.livermor.hot8app.feature.learning.train_fragments.communication.IWritingHelper
import io.reactivex.Observable
import kotlinx.android.synthetic.main.learn_writing_frag.*


open class WritingFragment : BaseTrainFrag<IWritingHelper>() {

    companion object {
        private const val TAG = "WritingFragment"
        private const val ARG_WORD = "ARG_WORD"
        private const val ARG_TRAN = "ARG_WORD_TRAN"
        private const val ARG_POS = "ARG_POS"

        fun instance(word: String, trans: String, pos: Int) = WritingFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_WORD, word)
                putString(ARG_TRAN, trans)
                putInt(ARG_POS, pos)
            }
        }
    }


    open protected val pos: Int by lazy(N) { arguments.getInt(ARG_POS) }
    open protected val word: String by lazy(N) { arguments.getString(ARG_WORD) }
    open protected val wordTrans: String by lazy(N) { arguments.getString(ARG_TRAN) }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.learn_writing_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        setUpLogic()
    }

    protected fun setUpView() {
        writing_word_text.text = wordTrans
    }

    protected fun setUpLogic() = with(helper) {
        writing_change_mode.setOnClickListener { changeProficientMode() }
        writing_answer_button.setOnClickListener {
            val result = writtenWordConfirmed(writing_word_edit_text.text.toString(), word, pos)
            onResultRight(result)
        }
        isRightLetter(word, getChangingTextObservable()).bindUi { isOk ->
            Log.i(TAG, "setUpLogic: word is written ok -> $isOk")
        }

        setUpWritingBoard()
    }

    private fun setUpWritingBoard() {
        word.forEach { char ->
            val textView = TextView(context).apply {
                setBackgroundColor(Color.BLUE)
                text = char.toString()
                gravity = Gravity.CENTER
                setOnClickListener {
                    val isRightChar = helper.isRightLetter(char, writing_word_edit_text.text.toString(), word)
                    onCharPressed(isRightChar)
                }
            }

            val lp = GridLayout.LayoutParams().apply {
                width = context.getDimen(R.dimen.train_char_width)
                height = context.getDimen(R.dimen.train_char_width)
                topMargin = (25 * context.dencity()).toInt()
                columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f)
                setGravity(Gravity.CENTER_HORIZONTAL)
            }

            writing_letters_container.addView(textView, lp)
        }
    }

    private fun TextView.onCharPressed(isRight: Boolean) {
        if (isRight) {
            visibleNotInvisible(false)
            setOnClickListener(null)
            writing_word_edit_text.setText(writing_word_edit_text.text.toString() + text.toString())
        } else {
            setBackgroundColor(Color.RED)
            postDelayed({ setBackgroundColor(Color.BLUE) }, 1000)
        }
    }

    protected fun getChangingTextObservable(): Observable<String> {
        val changingTextObservable = writing_word_edit_text.observableFromEditText()
        changingTextObservable.bindUi { text: String ->
            val buttonText = if (text.isEmpty()) "не знаю" else "ответить"
            writing_answer_button.text = buttonText
        }
        return changingTextObservable
    }

    private fun onResultRight(isRight: Boolean) {
        writing_word_edit_text.setTextColor(if (isRight) Color.GREEN else Color.RED)
    }
}