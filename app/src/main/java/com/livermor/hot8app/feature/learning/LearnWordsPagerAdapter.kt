package com.livermor.hot8app.feature.learning

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.PagerAdapter
import android.util.Log
import com.livermor.hot8app.domain.learn_words.Training
import com.livermor.hot8app.feature.common.view.view_pager.FixedFragmentStatePagerAdapter
import com.livermor.hot8app.feature.learning.train_fragments.frags.ListeningFragment
import com.livermor.hot8app.feature.learning.train_fragments.frags.ReadWordsFragment
import com.livermor.hot8app.feature.learning.train_fragments.frags.TransFragment
import com.livermor.hot8app.feature.learning.train_fragments.frags.WritingFragment
import com.livermor.hot8app.feature.learning.train_fragments.communication.*
import java.util.*


class LearnWordsPagerAdapter(fm: FragmentManager) :
        FixedFragmentStatePagerAdapter(fm), TrainAdapterTrait{

    companion object {
        private const val TAG = "LearnWordsPagerAdapter"
    }

    var trainData: List<TrainData> = Collections.emptyList()

    override fun getItem(position: Int): Fragment {
        return trainData[position].getFrag(position)
    }

    override fun getCount(): Int = trainData.size

    override fun getTag(position: Int): String {
        val tag = trainData[position].toString()
        Log.i(TAG, "getItem: creating tag $tag")
        return tag
    }

    override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE


    private fun TrainData.getFrag(pos: Int): Fragment {
        return when (this) {
            is ReadingData -> ReadWordsFragment.instance(wordToTranslation)
            is WritingData -> {
                if (training == Training.WRITING) WritingFragment.instance(word(), trans(), pos)
                else ListeningFragment.instance(word(), trans(), pos)
            }
            is TransData -> {
                if (training == Training.TRANS_FROM_ENG) {
                    TransFragment.instance(word(), trans(), true, translations, pos)
                } else {
                    TransFragment.instance(trans(), word(), false, translations, pos)
                }
            }
            else -> throw UnsupportedOperationException()
        }
    }

}