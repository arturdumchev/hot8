package com.livermor.hot8app.feature.common.base

import io.reactivex.Observable


interface IBaseVM {
    fun errorEmitter(): Observable<String>
    fun end()
}