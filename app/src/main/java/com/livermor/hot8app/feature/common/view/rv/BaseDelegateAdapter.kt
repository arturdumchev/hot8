package com.livermor.hot8app.feature.common.view.rv

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.livermor.hot8app.feature.common.view.rv.IDelegateAdapter
import com.livermor.hot8app.feature.common.view.rv.IDelegateViewType
import com.livermor.hot8app.feature.common.inflate


abstract class BaseDelegateAdapter<in T : IDelegateViewType>() : IDelegateAdapter {

    abstract val itemLayoutId: Int
    abstract fun View.onItemInflated(item: T): Unit

    override fun onCreateViewHolder(parent: ViewGroup) =
            BaseViewHolder(parent.inflate(itemLayoutId),
                    { i, v -> v.onItemInflated(i as T) })

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder,
                                  item: IDelegateViewType) {
        (holder as BaseViewHolder).bind(item)
    }

    class BaseViewHolder(parent: View,
                         private val onItemInflated: (IDelegateViewType, View) -> Unit) :
            RecyclerView.ViewHolder(parent) {
        fun bind(item: IDelegateViewType) = onItemInflated.invoke(item, itemView)
    }
}