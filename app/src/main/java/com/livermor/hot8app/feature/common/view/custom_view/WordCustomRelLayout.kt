package com.livermor.hot8app.feature.common.view.custom_view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.livermor.hot8app.R
import kotlinx.android.synthetic.main.custom_word.view.*


class WordCustomRelLayout : RelativeLayout {

    companion object {
        private val TAG = WordCustomRelLayout::class.java.simpleName
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    fun init() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_word, this, true)
    }

    fun setText1(text: String) {
        word_first.text = text
    }

    fun setText2(text: String) {
        word_second.text = text
    }

}
