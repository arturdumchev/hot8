package com.livermor.hot8app.feature.common.view.custom_view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N


class TextImage : TextView {

    companion object {
        private val TAG = TextImage::class.java.simpleName
    }

    private var imgWidth = 0f
    private var imgHeight = 0f
    private var imgRes = R.mipmap.ic_launcher
    private val img: Bitmap by lazy(N) {
        val bmp = BitmapFactory.decodeResource(resources, imgRes)
        Bitmap.createScaledBitmap(bmp, imgWidth.toInt(), imgHeight.toInt(), false)
    }
    private val imgPaint by lazy(N) { Paint(Paint.ANTI_ALIAS_FLAG) }

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {

        val attributeValuesStyle = getContext().theme
                .obtainStyledAttributes(attrs, R.styleable.TextImage, 0, 0)
        try {
            imgWidth = attributeValuesStyle.getDimension(R.styleable.TextImage_img_width, 0f)
            imgHeight = attributeValuesStyle.getDimension(R.styleable.TextImage_img_height, 0f)
            imgRes = attributeValuesStyle.getResourceId(R.styleable.TextImage_img_res, imgRes)
        } finally {
            attributeValuesStyle.recycle()
        }

        init()
    }

    fun init() {
        gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
        img
        imgPaint
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width: Int = Math.max(measuredWidth, imgWidth.toInt())
        val height: Int = (measuredHeight + imgHeight).toInt()
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val x = (measuredWidth - imgWidth) / 2
        val y = imgWidth / 20
        canvas.drawBitmap(img, x, y, imgPaint)
    }
}