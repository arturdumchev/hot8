package com.livermor.hot8app.feature.learning.train_fragments.frags

import android.content.Context
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.base.BaseFragment
import com.livermor.hot8app.feature.learning.train_fragments.communication.ITrainHelper


open class BaseTrainFrag<T : ITrainHelper> : BaseFragment() {

    interface Callback<out T : ITrainHelper> {
        fun getHelper(): T
    }

    protected var callback: Callback<T>? = null
    protected val helper: T by lazy(N) { callback!!.getHelper() }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context as Callback<T>
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }
}