package com.livermor.hot8app.feature.repetition

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.inflate
import com.livermor.hot8app.feature.learning.train_fragments.frags.TransFragment
import kotlinx.android.synthetic.main.repetition_frag.*
import java.util.*


class RepetitionFragment : TransFragment() {
    companion object {
        private val TAG = RepetitionFragment::class.java.simpleName

        private const val ARG_WORD = "ARG_REP_WORD"
        private const val ARG_TRANS_RIGHT = "ARG_REP_TRANS"
        private const val ARG_FROM_ENG = "ARG_REP_FROM_ENG"
        private const val ARG_POS = "ARG_REP_POS"
        private const val ARG_TRANS_VARS = "ARG_REP_TRANS_VARS"

        fun instance(word: String,
                     trans: String,
                     fromEng: Boolean,
                     translations: List<String>,
                     pos: Int) = RepetitionFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_WORD, word)
                putString(ARG_TRANS_RIGHT, trans)
                putBoolean(ARG_FROM_ENG, fromEng)
                putStringArrayList(ARG_TRANS_VARS, ArrayList(translations))
                putInt(ARG_POS, pos)
            }
        }
    }

    override val pos: Int by lazy(N) { arguments.getInt(ARG_POS) }
    override val mainWord: String by lazy(N) { arguments.getString(ARG_WORD) }
    override val mainTrans: String by lazy(N) { arguments.getString(ARG_TRANS_RIGHT) }
    override val isFromEng: Boolean by lazy(N) { arguments.getBoolean(ARG_FROM_ENG) }
    override val translations: List<String> by lazy(N) { arguments.getStringArrayList(ARG_TRANS_VARS) }

    override val textViews by lazy(N) { arrayListOf(rep_first_var_text, rep_second_var_text) }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.repetition_frag)

    override fun setUpLogic() {
        rep_word.text = mainWord
        setUpTranslations()
    }

    private fun setUpTranslations() {
        textViews.withIndex().forEach { it.value.setUpTranslationVar(translations[it.index]) }
    }

    private fun TextView.setUpTranslationVar(translation: String) {
        text = translation
        setOnClickListener {
            helper.chooseWord(translation, mainTrans, isFromEng, pos)
            this.onTransChosen()
        }

    }
}