package com.livermor.hot8app.feature.learning.train_fragments.communication

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.domain.learn_words.Training


interface TrainData {
    val training: Training
    var shouldContinue: Boolean
}

interface ProgressData : TrainData {
    val wordProgress: LearningProgress
    var attempt: Int
    var success: Boolean
}

data class ReadingData(
        override val training: Training = Training.READ_WORDS,
        val wordToTranslation: Map<String, String>,
        override var shouldContinue: Boolean = true) : TrainData

data class TransData(
        override val training: Training,
        override val wordProgress: LearningProgress,
        val translations: List<String>,
        override var shouldContinue: Boolean = true,
        override var attempt: Int = 0,
        override var success: Boolean = false) : ProgressData

data class WritingData(
        override val training: Training,
        override val wordProgress: LearningProgress,
        override var shouldContinue: Boolean = true,
        override var attempt: Int = 0,
        override var success: Boolean = false) : ProgressData