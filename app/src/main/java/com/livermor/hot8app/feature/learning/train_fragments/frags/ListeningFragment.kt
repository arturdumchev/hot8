package com.livermor.hot8app.feature.learning.train_fragments.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.livermor.hot8app.R
import com.livermor.hot8app.common.N
import com.livermor.hot8app.feature.common.inflate
import com.livermor.hot8app.feature.learning.train_fragments.frags.WritingFragment


class ListeningFragment : WritingFragment() {

    companion object {
        private const val TAG = "WritingFragment"
        private const val ARG_WORD = "ARG_WORD"
        private const val ARG_TRAN = "ARG_WORD_TRAN"
        private const val ARG_POS = "ARG_POS"

        fun instance(word: String, trans: String, pos: Int) = WritingFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_WORD, word)
                putString(ARG_TRAN, trans)
                putInt(ARG_POS, pos)
            }
        }
    }


    override val pos: Int by lazy(N) { arguments.getInt(ARG_POS) }
    override val word: String by lazy(N) { arguments.getString(ARG_WORD) }
    override val wordTrans: String by lazy(N) { arguments.getString(ARG_TRAN) }

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.learn_listening_frag)
}