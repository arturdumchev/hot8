package com.livermor.hot8app

import android.app.Application
import com.livermor.hot8app.common.model.Topic
import com.livermor.hot8app.data.realm.RealmInitializer
import io.realm.Realm


class App : Application() {

    companion object {
        private const val TAG = "App"
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
        initRealm()
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    private fun initRealm() {
        RealmInitializer.initRealm(this)


        // temp
        // choose Food eats be the favorite topic
        val realm = Realm.getDefaultInstance()
        val results = realm.where(Topic::class.java).equalTo("originalName", "Food").findAll()
        realm.executeTransaction { results.first().selected = true }

/*        val word = realm.where(Word::class.java).findAll().first()
        val rWord = realm.copyFromRealm(word)


        val learningProgress = rWord.toLeaningProgress()
        realm.executeTransaction { realm.copyToRealmOrUpdate(learningProgress) }

        val firstLearningProgress = realm.where(LearningProgress::class.java).findAll().first()
        println(firstLearningProgress.word.transcription)*/

        realm.close()
    }

/*    private fun Word.toLeaningProgress(): LearningProgress {
        val currentData = Date(System.currentTimeMillis())
        return LearningProgress(this, this.id, currentData, 0)
    }*/
}