package com.livermor.hot8app.data.preference.global

import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class GlobalDataModule {

    @Singleton
    @Provides
    fun provideGlobalData(prefs: SharedPreferences): IGlobalData = GlobalPrefs(prefs)
}