package com.livermor.hot8app.data.repo.words_progress

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.data.repo.RealmHelper
import io.realm.Realm


class ProgressRepo(private val realmHelper: RealmHelper = RealmHelper()) : IProgressRepo {

    companion object {
        private const val TAG = "ProgressRepo"
    }

    private val realm: Realm get() = realmHelper.realm

    override fun getWordsInProgress(): List<LearningProgress> {
        return realmHelper.getAll()
    }

    override fun saveWordsToLearn(learningProgress: List<LearningProgress>) {

        realm.executeTransaction { realm.copyToRealmOrUpdate(learningProgress) }
    }

    override fun getClearWords(amount: Int): List<LearningProgress> {
        val wordsQuery = realm.where(LearningProgress::class.java)
                .equalTo("progress", 0)
                .findAll()
        return realm.copyFromRealm(wordsQuery.subList(0, amount))
    }

    override fun saveIsLearned(word: LearningProgress, trainRaw: TrainingType) {
        word.progress = trainRaw.setTrue(word.progress)
        realm.executeTransaction { realm.copyToRealmOrUpdate(word) }
    }

    override fun setErrorMask(word: LearningProgress, trainRaw: TrainingType, isError: Boolean) {
        val errorsData = word.errors
        val newErrData = if (isError) trainRaw.setTrue(errorsData) else trainRaw.setFalse(errorsData)
        word.errors = newErrData
        realm.executeTransaction { realm.copyToRealmOrUpdate(word) }
    }
}