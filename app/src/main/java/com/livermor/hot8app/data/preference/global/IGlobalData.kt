package com.livermor.hot8app.data.preference.global


interface IGlobalData {
    var userLevel: Int
    var wordsToLearn: Int

    var creationDateMillis: Long
    var lastVisitDateMillis: Long
    var visitsCount: Int
}