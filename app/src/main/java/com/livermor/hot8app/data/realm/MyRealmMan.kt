package com.livermor.hot8app.data.realm

import android.util.Log
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults
import java.util.*


object MyRealmMan {
    val TAG: String = MyRealmMan::class.java.simpleName

    //—————————————————————————————————————————————————————————————————————— GET
    inline fun <reified T> getAll(): List<T> where T : RealmObject {
        return getAll(T::class.java)
    }

    fun <T : RealmObject> getAll(clazz: Class<T>): List<T> {
        val realm = Realm.getDefaultInstance()
        val items = realm.where(clazz).findAll()
        Log.i(TAG, "items.size == ${items.size}")

        val resultList = realm.copyFromRealm(items)
        realm.close()
        return resultList
    }

    inline fun <reified T> getAll(field: String, value: String): List<T> where T : RealmObject =
            getAll(T::class.java, field, value)


    fun <T : RealmObject> getAll(clazz: Class<T>, field: String, value: String): List<T> {
        val realm = Realm.getDefaultInstance()
        val items = realm.where(clazz).equalTo(field, value).findAll()
        val result = realm.copyFromRealm(items)
        Log.i(TAG, "getAll: items.size == ${result.size}")
        realm.close()
        return result
    }

    //—————————————————————————————————————————————————————————————————————— REPLACE

    fun <T : RealmObject> replaceAllWithNew(clazz: Class<T>, newList: List<T>) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            it.where(clazz).findAll().deleteAllFromRealm()
            for (item in newList) it.copyToRealm(item)
        }
        realm.close()
    }

    inline fun <reified T : RealmObject> replaceAllWithNew(newList: List<T>) {
        replaceAllWithNew(T::class.java, newList)
    }

    //—————————————————————————————————————————————————————————————————————— UPDATE

    inline fun <reified T : RealmObject> saveOrUpdate(items: List<T>) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            it.copyToRealmOrUpdate(items)
        }
        realm.close()
    }


    fun <T : RealmObject> updateIfAnyChanges(oldList: List<T>,
                                             newList: List<T>,
                                             findRule: (Realm) -> RealmResults<T>,
                                             deleteRule: (T) -> Unit
    ) {

        val newHashSet: HashSet<T> = HashSet(newList)

        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {

            findRule.invoke(it)
                    .filter { !newHashSet.contains(it) } //be sure of how equals() works
                    .forEach { deleteRule.invoke(it) }

            newHashSet.removeAll(oldList) // only really new items will remain
            newHashSet.forEach { realm.copyToRealmOrUpdate(it) }
        }

        realm.close()
    }

    fun <T : RealmObject> updateIfAnyChanges(clazz: Class<T>,
                                             oldList: List<T>,
                                             newList: List<T>,
                                             findRule: (Realm) -> RealmResults<T>,
                                             deleteRule: (T) -> Unit
    ) {

        val newHashSet: HashSet<T> = HashSet(newList)

        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {

            Log.w(TAG, "${clazz.simpleName}:size before deletion == ${findRule.invoke(it).size}")
            findRule.invoke(it)
                    .filter { !newHashSet.contains(it) } //be sure of how equals() works
                    .forEach { deleteRule.invoke(it) }

            newHashSet.removeAll(oldList) // only really new items will remain
            Log.w(TAG, "size after deletion == ${findRule.invoke(it).size}")

            //newHashSet.forEach { realm.copyToRealmOrUpdate(it) }
            Log.i(TAG, "newHashSet.s")
            realm.copyToRealmOrUpdate(newHashSet)
            Log.w(TAG, "size after adding == ${findRule.invoke(it).size}")
        }

        realm.close()
    }
}