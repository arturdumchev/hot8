package com.livermor.hot8app.data.repo.words

import com.livermor.hot8app.common.model.Word


interface IWordRepo {
    fun getRandomWords(amount: Int): List<Word>
}