package com.livermor.hot8app.data.repo

import io.realm.Realm
import io.realm.RealmObject


open class RealmHelper {
    val realm: Realm by lazy { Realm.getDefaultInstance() }
    private val TAG = "RealmHelper"

    //—————————————————————————————————————————————————————————————————————— get

    inline fun <reified T> getAll(): List<T> where T : RealmObject = getAll(T::class.java)
    open fun <T : RealmObject> getAll(clazz: Class<T>): List<T> {
        val items = realm.where(clazz).findAll()
        return realm.copyFromRealm(items)
    }


    inline fun <reified T : RealmObject> getAll(field: String, value: Boolean): List<T> = getAll(T::class.java, field, value)
    open fun <T : RealmObject> getAll(clazz: Class<T>, field: String, value: Boolean): List<T> {
        val realm = Realm.getDefaultInstance()
        val items = realm.where(clazz).equalTo(field, value).findAll()
        return realm.copyFromRealm(items)
    }

    //—————————————————————————————————————————————————————————————————————— replace


    inline fun <reified T : RealmObject> replaceAllWithNew(newList: List<T>) = replaceAllWithNew(T::class.java, newList)
    fun <T : RealmObject> replaceAllWithNew(clazz: Class<T>, newList: List<T>) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction {
            it.where(clazz).findAll().deleteAllFromRealm()
            it.copyToRealm(newList)
        }
        realm.close()
    }


}