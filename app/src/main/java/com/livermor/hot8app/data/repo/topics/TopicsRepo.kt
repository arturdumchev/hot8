package com.livermor.hot8app.data.repo.topics

import com.livermor.hot8app.common.model.Topic
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.data.repo.RealmHelper


class TopicsRepo(private val realmHelper: RealmHelper = RealmHelper()) : ITopicsRepo {

    override fun wordsFromTopics(): List<Word> {
        return realmHelper.getAll<Topic>("selected", true).flatMap { it.words }
    }

    override fun getTopics(): List<Topic> {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveTopics(topics: List<Topic>) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}