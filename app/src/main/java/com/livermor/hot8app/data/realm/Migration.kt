package com.livermor.hot8app.data.realm

import android.util.Log
import io.realm.DynamicRealm
import io.realm.RealmMigration


object Migration : RealmMigration {
    private const val TAG = "Migration"

    override fun migrate(realm: DynamicRealm, old: Long, new: Long) {
        Log.i(TAG, "migration: oldV $old, newV $new")

        // DynamicRealm exposes an editable schema
        val schema = realm.schema
        var oldVersion = old
        var newVersion = new

        if (oldVersion == 11L) {
            //schema.create("Person").addField("name", String::class.java).addField("age", Integer.TYPE)
                    //.removeField("id")
            schema.get("LearningProgress")
                    .addField("progress", Int::class.java)
                    .addField("errors", Int::class.java)
            oldVersion++
        }
/*
            // Migrate eats version 2: Add a primary key + object references
            // Example:
            // public Person extends RealmObject {
            //     private String name;
            //     @PrimaryKey
            //     private int age;
            //     private Dog favoriteDog;
            //     private RealmList<Dog> dogs;
            //     // getters and setters left out for brevity
            // }
            if (newVersion == 1L) {
//                        schema.get("Person").addField("id", java.lang.Long.TYPE, FieldAttribute.PRIMARY_KEY)
//                              .addRealmObjectField("favoriteDog", schema.get("Dog"))
//                              .addRealmListField("dogs", schema.get("Dog"))
//                        oldVersion++
            }
            */
    }
}