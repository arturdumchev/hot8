package com.livermor.hot8app.data.repo

import com.livermor.hot8app.data.repo.topics.ITopicsRepo
import com.livermor.hot8app.data.repo.topics.TopicsRepo
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.data.repo.words.WordRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.data.repo.words_progress.ProgressRepo
import com.livermor.hot8app.feature.common.injection.learner.Learner
import dagger.Module
import dagger.Provides


@Module
class LearnerDataModule {

    @Learner
    @Provides
    fun provideProgressRepo(): IProgressRepo = ProgressRepo()

    @Learner
    @Provides
    fun provideTopicsRepo(): ITopicsRepo = TopicsRepo()

    @Learner
    @Provides
    fun provideWordRepo(): IWordRepo = WordRepo()
}