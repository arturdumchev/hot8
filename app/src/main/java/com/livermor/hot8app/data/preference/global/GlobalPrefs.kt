package com.livermor.hot8app.data.preference.global

import android.content.SharedPreferences
import com.livermor.hot8app.data.LangLevel
import com.livermor.hot8app.data.preference.PrefsDelegate


/**
 * All rx data classes based on
 * @see com.livermor.hot8app.data.preference.PrefsDelegate
 * which helps to create BehaviorSubjects, which is based on SharedPreferences
 */
class GlobalPrefs(prefs: SharedPreferences) : IGlobalData, PrefsDelegate(prefs) {

    companion object {
        private const val TAG = "GlobalRxPrefs"

        private const val PRF_USER_LEVEL = "USER_LEVEL preference key"
        private const val PRF_WORDS_TO_LEARN = "USER_LEVEL preference key"

        private const val PRF_VISITS_COUNT = "PRF_VISITS_COUNT"
        private const val PRF_FIRST_CREATION_DATE = "PRF_FIRST_CREATION_DATE"
        private const val PRF_LAST_VISIT_DATE = "PRF_LAST_VISIT_DATE"

    }

    override var userLevel: Int by delegate(PRF_USER_LEVEL, LangLevel.MIDDLE.num)
    override var wordsToLearn: Int by delegate(PRF_WORDS_TO_LEARN, 0)


    override var creationDateMillis: Long by delegate(PRF_FIRST_CREATION_DATE, System.currentTimeMillis())
    override var lastVisitDateMillis: Long by delegate(PRF_LAST_VISIT_DATE, 0L)
    override var visitsCount: Int by delegate(PRF_VISITS_COUNT, 0)
}
