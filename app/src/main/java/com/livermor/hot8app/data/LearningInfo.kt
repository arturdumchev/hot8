package com.livermor.hot8app.data


enum class LangLevel(val num: Int) {
    START(1),
    BEGIN(2),
    MIDDLE(3),
    UPPER_MIDDLE(4),
    PROFICIENT(5),
    FLUENT(6)
}

