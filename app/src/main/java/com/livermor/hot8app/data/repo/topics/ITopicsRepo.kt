package com.livermor.hot8app.data.repo.topics

import com.livermor.hot8app.common.model.Topic
import com.livermor.hot8app.common.model.Word


interface ITopicsRepo  {
    fun wordsFromTopics(): List<Word>

    fun getTopics(): List<Topic>
    fun saveTopics(topics: List<Topic>)
}