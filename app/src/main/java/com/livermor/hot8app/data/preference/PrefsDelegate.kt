package com.livermor.hot8app.data.preference

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


open class PrefsDelegate(protected val prefs: SharedPreferences) {

    inline protected fun <R, reified T> delegate(key: String, initial: T): ReadWriteProperty<R, T> {
        return object : ReadWriteProperty<R, T> {
            var t: T = getPrefsValue(key, initial)
            override fun getValue(thisRef: R, property: KProperty<*>): T = t
            override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
                t = value
                editPrefs(key, value)
            }
        }
    }

    inline protected fun <reified T> getPrefsValue(key: String, initial: T): T =
            when (T::class.simpleName) {
                Int::class.simpleName -> prefs.getInt(key, initial as Int) as T
                String::class.simpleName -> prefs.getString(key, initial as String) as T
                Long::class.simpleName -> prefs.getLong(key, initial as Long) as T
                Float::class.simpleName -> prefs.getFloat(key, initial as Float) as T
                Boolean::class.simpleName -> prefs.getBoolean(key, initial as Boolean) as T
                else -> throw UnsupportedOperationException()
            }

    inline protected fun <reified T> editPrefs(key: String, value: T) {
        val editor: SharedPreferences.Editor
        when (T::class.simpleName) {
            Int::class.simpleName -> editor = prefs.edit().putInt(key, value as Int)
            String::class.simpleName -> editor = prefs.edit().putString(key, value as String)
            Long::class.simpleName -> editor = prefs.edit().putLong(key, value as Long)
            Float::class.simpleName -> editor = prefs.edit().putFloat(key, value as Float)
            Boolean::class.simpleName -> editor = prefs.edit().putBoolean(key, value as Boolean)
            else -> throw UnsupportedOperationException()
        }
        editor.apply()
    }
}