package com.livermor.hot8app.data.repo.words_progress

import com.livermor.hot8app.common.model.LearningProgress


interface IProgressRepo {
    fun getWordsInProgress(): List<LearningProgress>
    fun saveWordsToLearn(learningProgress: List<LearningProgress>)

    fun getClearWords(amount: Int): List<LearningProgress>

    fun saveIsLearned(word: LearningProgress, trainRaw: TrainingType)
    fun setErrorMask(word: LearningProgress, trainRaw: TrainingType, isError: Boolean)
}