package com.livermor.hot8app.data.repo.words

import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.data.repo.RealmHelper
import io.realm.Realm
import java.util.*


class WordRepo(private val realmHelper: RealmHelper = RealmHelper()) : IWordRepo {

    companion object {
        private val TAG = WordRepo::class.java.simpleName
    }

    private val random: Random
    private val realm: Realm get() = realmHelper.realm

    init {
        random = Random()
    }

    override fun getRandomWords(amount: Int): List<Word> {
        return getRandWords(amount, realm.where(Word::class.java).findAll())
    }

    private fun getRandWords(amount: Int, allWords: List<Word>): List<Word> =
            HashSet<Int>().apply {
                while (size < amount) {
                    add(random.nextInt(allWords.size))
                }
            }.map { allWords[it] }
}