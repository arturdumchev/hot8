package com.livermor.hot8app.data.realm

import android.content.Context
import android.util.Log
import com.livermor.hot8app.R
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.internal.IOException
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

// first version is 11
object RealmInitializer {


    private const val TAG = "RealmInitializer"
    const val REALM_FILE_NAME = "default0.realm"
    const val CURRENT_VERSION = 11L

    private val config: RealmConfiguration
        get() = RealmConfiguration.Builder()
                .name(REALM_FILE_NAME)
                .schemaVersion(CURRENT_VERSION)
                .migration(Migration)
                .build()

    fun getRealm(): Realm = Realm.getInstance(config)

    fun initRealm(context: Context) = with(context) {
        Realm.init(this)
        Realm.setDefaultConfiguration(config)
        val realm = Realm.getDefaultInstance()

        if (realm.isEmpty) {
            Log.i(TAG, "initRealm: realmIsEmpty")
            realm.close()
            copyBundledRealmFile(this, resources.openRawResource(R.raw.ru_en_default2), REALM_FILE_NAME)
            //Realm.migrateRealm(config, Migration)
        }


        //Realm.setDefaultConfiguration(config)
    }


    private fun copyBundledRealmFile(
            context: Context,
            inputStream: InputStream,
            outFileName: String): String? {

        try {
            val file = File(context.filesDir, outFileName)
            val outputStream = FileOutputStream(file)
            val buf = ByteArray(1024)
            var bytesRead = inputStream.read(buf)

            while (bytesRead > 0) {
                outputStream.write(buf, 0, bytesRead)
                bytesRead = inputStream.read(buf)
            }

            outputStream.close()
            return file.absolutePath
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }
}