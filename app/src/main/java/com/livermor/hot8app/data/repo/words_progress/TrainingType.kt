package com.livermor.hot8app.data.repo.words_progress


enum class TrainingType(private val bit: Int) {
    QUICK_BRAIN(1), // so called "быстрая тренировка"
    CHOOSE_WORD(1 shl 1), // and so on "Выбор перевода из 4"
    CHOOSE_TRANSLATION(1 shl 2), // "Выбор слова из 4"
    WRITING(1 shl 3), // "Аудирование"
    LISTENING(1 shl 4);           // "Ввод перевода"

    companion object {
        fun isAllTypesClear(data: Int): Boolean =
                TrainingType.values().filter { it.getValue(data) }.firstOrNull() == null
    }

    fun setFalse(data: Int): Int = data and bit.inv()
    fun setTrue(data: Int): Int = data or bit
    fun getValue(data: Int): Boolean = data and bit != 0

    //fun isEverythingFalse(data: Int) = data == 0
}