package com.livermor.hot8app.common.util

import java.util.*


object MapArrayConverter {

    fun getMap(list: Array<String>): Map<String, String> {
        val map = HashMap<String, String>()
        val half = list.size / 2
        for (i in 0..half - 1) {
            map.put(list[i + half], list[i])
        }
        return map
    }

    fun getArr(map: Map<String, String>): Array<String> {
        return (map.values + map.keys).toTypedArray()
    }

}