package com.livermor.hot8app.common.util

import java.text.SimpleDateFormat
import java.util.*


object DateHelper {
    const val DAY = 86400000L

    private val calendar by lazy(LazyThreadSafetyMode.NONE) { Calendar.getInstance() }





    fun daysBetween(d1: Date, d2: Date): Long {
        return ((d2.time - d1.time) / (1000 * 60 * 60 * 24))
    }

    fun daysBetweenBasedOnDate(d1: Date, d2: Date): Long {
        return ((d2.time + DAY - 1 - d1.time) / (DAY))
    }

    fun isFirstDateInSecondDate(d1: Date, d2: Date): Boolean {
        val dayNum1 = dayNum(d1)
        val dayNum2 = dayNum(d2)
        return dayNum1 == dayNum2
    }

    fun dateFromString(inputFormat: String, dateString: String): Date {
        val df_input = SimpleDateFormat(inputFormat, Locale.ENGLISH)
        val date: Date = df_input.parse(dateString)

        return date
    }

    fun hourNum(date: Date): Int{
        calendar.time = date
        return calendar.get(Calendar.HOUR_OF_DAY)
    }

    fun dayNum(date: Date): Int {
        calendar.time = date
        return calendar.get(Calendar.DAY_OF_MONTH)
    }

    fun monthNum(date: Date): Int {
        calendar.time = date
        return calendar.get(Calendar.MONTH)
    }

    fun monthAndDay(date: Date): Pair<Int, Int> {
        calendar.time = date
        return Pair(calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

    fun datesBetweenInclusive(first: Date, second: Date): List<Date> {
        println("d1 $first, d2 $second")
        val daysAmount = daysBetweenBasedOnDate(first, second)
        val days = arrayListOf(first)
        for (i in 1..daysAmount) {
            days.add(Date(first.time + DAY * i))
        }
        return days
    }
}