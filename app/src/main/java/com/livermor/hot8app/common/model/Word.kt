package com.livermor.hot8app.common.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Word(
        @PrimaryKey open var id: Int = 0,
        open var translation: String = "",
        open var transcription: String = "",
        open var writing: String = "",
        open var skipped: Boolean = false,
        open var enabled: Boolean = true,
        open var level: Int = 0) : RealmObject() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Word) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}