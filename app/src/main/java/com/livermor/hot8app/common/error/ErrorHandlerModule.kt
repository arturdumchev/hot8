package com.livermor.hot8app.common.error

import android.content.Context
import com.livermor.hot8app.feature.common.injection.feature.Feature
import dagger.Module
import dagger.Provides


@Module
class ErrorHandlerModule {

    @Feature
    @Provides
    fun provideErrorHandler(context: Context): IErrorHandler = MyErrorHandler(context)
}