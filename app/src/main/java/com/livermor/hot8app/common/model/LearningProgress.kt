package com.livermor.hot8app.common.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*


open class LearningProgress(
        open var word: Word = Word(),
        @PrimaryKey open var id: Int = 0,
        @Required open var creationDate: Date = Date(),
        open var repetitionStep: Int = 0,
        open var repetitionDate: Date = Date(),
        open var onFastBrain: Boolean = false,
        open var repetitionFastBrainStep: Int = 0,
        open var repetitionFastBrainDate: Date = Date(),
        open var fastBrainWaitingDate: Date = Date(),
        open var fastBrainWaitingInFuture: Boolean = false,
        open var progress: Int = 0,
        open var errors: Int = 0
) : RealmObject()

/*
    progress
    errors

 */