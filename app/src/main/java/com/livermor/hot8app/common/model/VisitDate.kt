package com.livermor.hot8app.common.model

import io.realm.RealmObject
import java.util.*


/*
remove rawDate
 */
open class VisitDate(
        open var date: Date = Date(),
        open var rawDate: Date = Date()) : RealmObject()