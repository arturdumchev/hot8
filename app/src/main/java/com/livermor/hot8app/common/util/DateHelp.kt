package com.livermor.hot8app.common.util

import com.livermor.hot8app.data.preference.global.IGlobalData
import java.util.*


class DateHelp(private val globalData: IGlobalData) {

    companion object {
        private val TAG = DateHelp::class.java.simpleName
        const private val FOUR_HOURS: Long = 14400000
    }

    fun update() {
        val lastVisitDateMillis: Long = globalData.lastVisitDateMillis
        val lastDate = Date(lastVisitDateMillis)

        setCreationDateIfNeeded()
        updateVisitsCountIfNeeded(lastDate)
        setNewLastVisitTime()
    }

    private fun setCreationDateIfNeeded() {
        if (globalData.visitsCount == 0) {
            globalData.creationDateMillis = System.currentTimeMillis()
        }
    }

    private fun updateVisitsCountIfNeeded(lastDate: Date) {
        if (!isToday(lastDate)) {
            globalData.visitsCount = globalData.visitsCount + 1
        }
    }

    private fun isToday(date: Date): Boolean {
        val now = Date(System.currentTimeMillis())
        return getDayNum(now) == getDayNum(date)
    }

    private fun getDayNum(date: Date): Int {
        var dayNum = DateHelper.dayNum(date)
        val dayHour = DateHelper.hourNum(date)
        if (dayHour < 4) dayNum -= 1
        return dayNum
    }

    private fun setNewLastVisitTime() {
        globalData.lastVisitDateMillis = System.currentTimeMillis()
    }
}