package com.livermor.hot8app.common.util

import io.reactivex.CompletableEmitter
import io.reactivex.Emitter
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.reactivestreams.Subscriber


fun Disposable.addTo(disposable: CompositeDisposable) {
    disposable.add(this)
}


//—————————————————————————————————————————————————————————————————————— constructors
fun <T> bs() = BehaviorSubject.create<T>()

fun <T> ps() = PublishSubject.create<T>()
fun <T> bs(t: T): BehaviorSubject<T> {
    val bs = bs<T>()
    bs eats t
    return bs
}

//—————————————————————————————————————————————————————————————————————— eats operator

infix fun <T> Observer<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Subscriber<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> BehaviorSubject<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Emitter<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Emitter<T>.eats(t: Throwable) {
    this.onError(t)
}

infix fun CompletableEmitter.eats(throwable: Throwable) {
    this.onError(throwable)
}