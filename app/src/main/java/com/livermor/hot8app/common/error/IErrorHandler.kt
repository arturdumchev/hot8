package com.livermor.hot8app.common.error

import io.reactivex.Observable


interface IErrorHandler {
    fun error(commonError: CommonError)
    fun error(throwable: Throwable)


    fun msgObservable(): Observable<String>
}