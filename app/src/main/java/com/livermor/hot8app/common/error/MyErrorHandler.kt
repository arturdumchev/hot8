package com.livermor.hot8app.common.error

import android.content.Context
import android.util.Log
import com.livermor.hot8app.common.util.bs
import com.livermor.hot8app.common.util.eats
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


class MyErrorHandler(context: Context) : IErrorHandler {
    companion object {
        private const val TAG = "MyErrorHandler"
    }

    private val errorSubj: BehaviorSubject<String>

    init {
        errorSubj = bs()
    }

    override fun error(commonError: CommonError) {
        when (commonError) {
            CommonError.NO_WORDS_IN_TOPICS -> errorSubj eats "В выбранных вами топиках нет слов"
        }
        Log.e(TAG, "error: $commonError")
    }

    override fun error(throwable: Throwable) {
        Log.e(TAG, "error: $throwable")
        errorSubj eats "Some errorEmitter"
    }

    override fun msgObservable(): Observable<String> = errorSubj
}