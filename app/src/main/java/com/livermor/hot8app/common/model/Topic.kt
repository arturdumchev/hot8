package com.livermor.hot8app.common.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Topic(
        @PrimaryKey open var id: Int = 0,
        open var originalName: String = "",
        open var translatedName: String = "",
        open var enabled: Boolean = true,
        open var order: Int = 0,
        open var selected: Boolean = false,
        open var words: RealmList<Word> = RealmList()) : RealmObject()