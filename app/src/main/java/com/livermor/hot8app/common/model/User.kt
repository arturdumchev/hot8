package com.livermor.hot8app.common.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*


open class User(
        @PrimaryKey @Required open var key: String = "",
        open var creationDate: Date = Date(),
        open var visits: RealmList<VisitDate> = RealmList(),
        open var languageLevel: Int? = null) : RealmObject()