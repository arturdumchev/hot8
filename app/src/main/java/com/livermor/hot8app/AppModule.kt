package com.livermor.hot8app

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideApp(): App = app

    @Provides
    @Singleton
    fun providePrefs(): SharedPreferences = app.getSharedPreferences(app.packageName, Context.MODE_PRIVATE)
}