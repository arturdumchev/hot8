package com.livermor.hot8app

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.livermor.hot8app.feature.MainActivity

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        startActivity(Intent(this, MainActivity::class.java))
        //startActivity(Intent(this, LearnWordsActivity::class.java))
    }

}
