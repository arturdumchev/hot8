package com.livermor.hot8app.domain.words_for_future

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.data.preference.global.IGlobalData
import com.livermor.hot8app.data.repo.topics.ITopicsRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.domain.base.BaseInteractor
import io.reactivex.Single
import java.util.*


class WordsInteractor(private val progressRepo: IProgressRepo,
                      private val topicsRepo: ITopicsRepo,
                      private val globalData: IGlobalData) :
        IWordsInteractor, BaseInteractor() {

    companion object {
        private const val TAG = "WordsToLearnInteractor"
    }


    override fun getWordsToLearn(): Single<List<Word>> {
        return Single.create { sub ->
            val words = topicsRepo.wordsFromTopics() - progressRepo.getWordsInProgress().map { it.word }
            val userLevelWords = words.filter { it.level == globalData.userLevel }

            // return getWords from different level, if there is 0 level getWords need eats know, how eats do in right way
            val result = if (userLevelWords.isNotEmpty()) userLevelWords else words
            sub.onSuccess(result)
        }
    }

    override fun addWordsForLearning(words: Set<Word>) {

        val wordsToLearn = globalData.wordsToLearn
        globalData.wordsToLearn = wordsToLearn + words.size

        progressRepo.saveWordsToLearn(words.map { it.toLeaningProgress() })
    }

    private fun Word.toLeaningProgress(): LearningProgress {
        val currentData = Date(System.currentTimeMillis())
        return LearningProgress(this, this.id, currentData, 0)
    }

}