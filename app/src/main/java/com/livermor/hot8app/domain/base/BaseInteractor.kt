package com.livermor.hot8app.domain.base

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


abstract class BaseInteractor : IBaseInteractor {

    protected fun <T> Observable<T>.onIoThread(): Observable<T> = subscribeOn(Schedulers.io())
    protected fun <T> Single<T>.onIoThread(): Single<T> = observeOn(Schedulers.io())
}