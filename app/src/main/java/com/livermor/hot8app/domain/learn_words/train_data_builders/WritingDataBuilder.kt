package com.livermor.hot8app.domain.learn_words.train_data_builders

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.domain.learn_words.Training
import com.livermor.hot8app.feature.learning.train_fragments.communication.WritingData
import java.util.*


class WritingDataBuilder() {

    fun getList(trainProgressList: List<LearningProgress>): List<WritingData> {
        val writingData = ArrayList<WritingData>()
        val listeningData = ArrayList<WritingData>()
        trainProgressList.forEach { word ->
            writingData.add(word.createWritingData(false))
            listeningData.add(word.createWritingData(true))
        }

        return writingData + listeningData
    }

    private fun LearningProgress.createWritingData(isListening: Boolean): WritingData {
        val type = if (isListening) Training.LISTENING else Training.WRITING
        return WritingData(type, this)
    }

}