package com.livermor.hot8app.domain.learn_words

import com.livermor.hot8app.data.repo.words_progress.TrainingType


enum class Training {
    NONE,
    READ_WORDS,
    TRANS_INTO_ENG,
    TRANS_FROM_ENG,
    WRITING,
    LISTENING;


    fun getTrainType(): TrainingType = when (this) {
        TRANS_INTO_ENG -> TrainingType.CHOOSE_WORD
        TRANS_FROM_ENG -> TrainingType.CHOOSE_TRANSLATION
        WRITING -> TrainingType.WRITING
        LISTENING -> TrainingType.LISTENING
        NONE, READ_WORDS -> throw UnsupportedOperationException("NONE and READ_WORD doesn't have TrainingTypes")
    }

}

