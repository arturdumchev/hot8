package com.livermor.hot8app.domain

import com.livermor.hot8app.data.preference.global.IGlobalData
import com.livermor.hot8app.data.repo.topics.ITopicsRepo
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.domain.learn_words.ILearnInteractor
import com.livermor.hot8app.domain.learn_words.LearnInteractor
import com.livermor.hot8app.domain.words_for_future.IWordsInteractor
import com.livermor.hot8app.domain.words_for_future.WordsInteractor
import com.livermor.hot8app.feature.common.injection.learner.Learner
import dagger.Module
import dagger.Provides


@Module
class LearnerDomainModule {

    @Provides
    @Learner
    fun provideWordsInteracotr(progressRepo: IProgressRepo,
                               topicsRepo: ITopicsRepo,
                               globalData: IGlobalData): IWordsInteractor {
        return WordsInteractor(progressRepo, topicsRepo, globalData)
    }

    @Provides
    @Learner
    fun provideLearnInteractor(progressRepo: IProgressRepo,
                               wordsRepo: IWordRepo): ILearnInteractor {
        return LearnInteractor(progressRepo, wordsRepo)
    }
}