package com.livermor.hot8app.domain.learn_words

import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import io.reactivex.Single


interface ILearnInteractor {

    fun haveDoneEnoughRounds(roundsPassed: Int): Boolean
    fun haveLearnedEnoughPacks(packsLearned: Int): Boolean

    fun getTrainData(previousData: List<TrainData>): Single<List<TrainData>>

    fun saveTrainedData(dataList: List<TrainData>)
}