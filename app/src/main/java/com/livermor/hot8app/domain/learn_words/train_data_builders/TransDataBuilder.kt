package com.livermor.hot8app.domain.learn_words.train_data_builders

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.domain.learn_words.Training
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import java.util.*


class TransDataBuilder(private val wordsPerTrainAmount: Int,
                       private val wordRepo: IWordRepo) {
    companion object {
        private val TAG = TransDataBuilder::class.java.simpleName
    }

    private val rand: Random = Random()

    fun getList(trainProgressList: List<LearningProgress>): List<TransData> {

        val chooseWordsList = ArrayList<TransData>(wordsPerTrainAmount)
        val chooseTranslationList = ArrayList<TransData>(wordsPerTrainAmount)

        trainProgressList.forEach { word ->
            val randWords = wordRepo.getRandomWords(wordsPerTrainAmount * 2 - 2)
            chooseWordsList.add(word.buildTransData(true, randWords))
            chooseTranslationList.add(word.buildTransData(false, randWords))
        }
        return chooseWordsList + chooseTranslationList
    }

    private fun LearningProgress.buildTransData(isChooseWord: Boolean, randWords: List<Word>): TransData {

        val trainType = if (isChooseWord) Training.TRANS_INTO_ENG else Training.TRANS_FROM_ENG
        return TransData(trainType, this, randWords.getTranslations(isChooseWord, this))
    }

    private fun List<Word>.getTranslations(isChooseWord: Boolean, mainWord: LearningProgress): List<String> {
        val size = this.size / 2
        val resultList: MutableList<String> =
                if (isChooseWord) this.drop(size).map(Word::writing).toMutableList()
                else this.take(size).map(Word::translation).toMutableList()

        val inx = rand.nextInt(size + 1)
        val word = if (isChooseWord) mainWord.word.writing else mainWord.word.translation
        if (inx == size) resultList.add(word)
        else resultList.add(inx, word)

        assert(wordsPerTrainAmount == resultList.size)
        return resultList
    }

}