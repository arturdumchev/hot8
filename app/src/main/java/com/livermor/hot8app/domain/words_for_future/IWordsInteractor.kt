package com.livermor.hot8app.domain.words_for_future

import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.domain.base.IBaseInteractor
import io.reactivex.Single


interface IWordsInteractor : IBaseInteractor{
    fun getWordsToLearn(): Single<List<Word>>
    fun addWordsForLearning(words: Set<Word>)
}