package com.livermor.hot8app.domain.learn_words

import com.livermor.hot8app.common.N
import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.data.repo.words_progress.TrainingType
import com.livermor.hot8app.domain.ATTEMPTS_TO_LEARN
import com.livermor.hot8app.domain.LEAR_WORDS_PER_TRAIN
import com.livermor.hot8app.domain.WORDS_PACKS_TO_LEARN
import com.livermor.hot8app.domain.base.BaseInteractor
import com.livermor.hot8app.domain.learn_words.train_data_builders.TransDataBuilder
import com.livermor.hot8app.domain.learn_words.train_data_builders.WritingDataBuilder
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.communication.ReadingData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import io.reactivex.Single
import java.util.*

class LearnInteractor(private val progressRepo: IProgressRepo,
                      private val wordRepo: IWordRepo,
                      private val wordsPerTrain: Int = LEAR_WORDS_PER_TRAIN,
                      private val attempts: Int = ATTEMPTS_TO_LEARN,
                      private val packsToLean: Int = WORDS_PACKS_TO_LEARN) :
        ILearnInteractor, BaseInteractor() {

    private val TAG = LearnInteractor::class.java.simpleName

    private val transDataBuilder by lazy(N) { TransDataBuilder(wordsPerTrain, wordRepo) }
    private val writeDataBuilder by lazy(N) { WritingDataBuilder() }

    override fun haveLearnedEnoughPacks(packsLearned: Int): Boolean = packsLearned >= packsToLean
    override fun haveDoneEnoughRounds(roundsPassed: Int): Boolean = roundsPassed >= attempts

    override fun getTrainData(previousData: List<TrainData>): Single<List<TrainData>> {
        if (previousData.isEmpty()) return Single.just(createNewTrainData()).onIoThread()
        return Single.create { sub ->
            previousData.forEach { trainData: TrainData ->
                if (trainData is ProgressData) {
                    if (trainData.success) trainData.shouldContinue = false
                    if (trainData.wasteAttempts()) trainData.shouldContinue = false
                } else trainData.shouldContinue = false
            }
            sub.onSuccess(previousData)
        }
    }

    override fun saveTrainedData(dataList: List<TrainData>) {
        dataList.forEach { data ->
            if (data is ProgressData) {
                data.saveIfTrouble()
                data.saveIfLearned()
            }
        }
    }


    //—————————————————————————————————————————————————————————————————————— helpers

    private fun createNewTrainData(): List<TrainData> {
        val wordToLearnNow = progressRepo.getWordsInProgress()
                .filter { TrainingType.isAllTypesClear(it.progress) } // words that wasn't touched yet
                .take(wordsPerTrain)

        val readingData = getReadingData(wordToLearnNow)
        val transData = transDataBuilder.getList(wordToLearnNow)
        val writingData = writeDataBuilder.getList(wordToLearnNow)

        return listOf(readingData) + transData + writingData
    }

    private fun getReadingData(trainProgressList: List<LearningProgress>): ReadingData {
        println("trainProgressList.size == ${trainProgressList.size}")
        assert(trainProgressList.size == wordsPerTrain)
        val wordToTranslation = trainProgressList.fold(HashMap<String, String>()) { map, word ->
            map.apply { put(word.word.writing, word.word.translation) }
        }
        return ReadingData(Training.READ_WORDS, wordToTranslation)
    }


    //—————————————————————————————————————————————————————————————————————— train data extensions

    private fun ProgressData.saveIfLearned() {
        if (this.success) progressRepo.saveIsLearned(wordProgress, type())
    }

    private fun ProgressData.saveIfTrouble() {
        if (wasteAttempts()) progressRepo.setErrorMask(wordProgress, type(), true)
    }

    private fun ProgressData.wasteAttempts(): Boolean = haveDoneEnoughRounds(this.attempt)
    private fun TrainData.type(): TrainingType = this.training.getTrainType()
}