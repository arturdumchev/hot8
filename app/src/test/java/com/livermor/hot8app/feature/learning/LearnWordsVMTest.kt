package com.livermor.hot8app.feature.learning

import com.livermor.hot8app.common.mocks.MockedTrainData
import com.livermor.hot8app.common.util.eats
import com.livermor.hot8app.domain.learn_words.ILearnInteractor
import com.livermor.hot8app.feature.BaseVmTest
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import com.livermor.hot8app.feature.learning.train_fragments.communication.WritingData
import com.livermor.hot8app.mock
import com.livermor.hot8app.mustReturn
import com.livermor.hot8app.single
import com.livermor.hot8app.testOnNext
import io.reactivex.observers.TestObserver
import io.reactivex.subjects.BehaviorSubject
import org.junit.Test
import java.util.*


class LearnWordsVMTest : BaseVmTest() {

    companion object {
        private val trainData: List<TrainData> = MockedTrainData.allTrainData
    }

    private val interactor: ILearnInteractor = mock()
    private lateinit var trainDataObserver: TestObserver<List<TrainData>>
    private lateinit var positionObserver: TestObserver<Int>

    override val vm: LearnWordsVM by lazy { LearnWordsVM(errorHandler, interactor) }


    override fun setUp() {
        super.setUp()
        MockedTrainData.cleanData()
        trainDataObserver = TestObserver()
        positionObserver = TestObserver()

        interactor.getTrainData(Collections.emptyList()) mustReturn single(trainData)

        vm.getTrainData().subscribe(trainDataObserver)
    }

    @Test fun testScrollToPos() {
        vm.scrollToPosition().subscribe(positionObserver)

        trainData.withIndex().drop(1).forEach {
            val data = it.value
            if (it.index == trainData.size - 1) {
                interactor.getTrainData(trainData) mustReturn single(listOf())
            }

            completeTrainStep(true, it.index)

            if (it.index != trainData.size - 1) {
                assert(positionObserver.values().last() == it.index + 1)
            } else {
                assert(positionObserver.values().last() == 0)
            }
            println("next ${positionObserver.values().lastOrNull()}")
            println("index ${it.index}\n")
            println(positionObserver.values())
        }

        trainData.forEach { it.shouldContinue = false }
        (trainData[2] as ProgressData).shouldContinue = true
        (trainData[trainData.size - 1] as ProgressData).shouldContinue = true

        interactor.getTrainData(trainData) mustReturn single(trainData)
        vm.getTrainData()

        completeTrainStep(false, 1)
        assert(positionObserver.values().last() == 2)

        completeTrainStep(false, 2)
        assert(positionObserver.values().last() == trainData.size - 1)
    }

    private fun completeTrainStep(shouldChooseRight: Boolean, position: Int) {
        val data = trainData[position]
        if (data is TransData) vm.chooseWord("", "", shouldChooseRight, position)
        else if (data is WritingData) vm.writtenWordConfirmed("word", "word", position)
    }

    @Test fun getTrainData() {
        trainDataObserver.testOnNext(trainData)
    }


    @Test fun testWordIsGoingToBeWrittenRight() {
        val wordSubj = BehaviorSubject.create<String>()
        val word = "word"
        val wordRightObserver = TestObserver<Boolean>()
        vm.isRightLetter(word, wordSubj).subscribe(wordRightObserver)

        word.fold("") { text, char ->
            wordSubj eats text
            assert(wordRightObserver.values().last())
            text + char
        }

        wordSubj eats "word".toUpperCase()
        assert(wordRightObserver.values().last())

        wordSubj eats "anal"
        assert(!wordRightObserver.values().last())
    }
}