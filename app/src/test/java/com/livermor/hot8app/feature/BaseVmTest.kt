package com.livermor.hot8app.feature

import com.livermor.hot8app.common.ErrorHandlerMocked
import com.livermor.hot8app.data.preference.global.IGlobalData
import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.mocks.GlobalDataMocked
import com.livermor.hot8app.feature.common.base.IBaseVM
import com.livermor.hot8app.wait
import io.reactivex.observers.TestObserver
import org.junit.Before


abstract class BaseVmTest {

    protected val errorHandler: IErrorHandler = ErrorHandlerMocked
    protected val globalData: IGlobalData = GlobalDataMocked
    protected var errorObserver = TestObserver<String>()

    abstract val vm: IBaseVM

    @Before open fun setUp() {
        errorObserver = TestObserver<String>()
        vm.errorEmitter().subscribe(errorObserver)
    }

    protected fun errorMessagesCount(count: Int) {
        errorObserver.wait(30)
        errorObserver.assertNoErrors()
        errorObserver.assertValueCount(count)
        errorObserver.assertNotTerminated()
    }
}