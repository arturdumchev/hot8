package com.livermor.hot8app.feature.choose_words

import com.livermor.hot8app.*
import com.livermor.hot8app.common.mocks.MockedWords
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.domain.words_for_future.IWordsInteractor
import com.livermor.hot8app.feature.BaseVmTest
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test

class ChooseWordsVMTest : BaseVmTest() {

    private val interactor: IWordsInteractor = mock()

    override val vm: IChooseWordsVM = ChooseWordsVM(errorHandler, interactor)

    private val words = MockedWords.wordsToChooseForLearning
    private val chosenWords = words.toSet()
    private var wasWordChosen = false

    private var wordsObserver = TestObserver<List<Word>>()
    private var autoSlideObserver = TestObserver<Unit>()

    @Before override fun setUp() {
        super.setUp()
        interactor.getWordsToLearn() mustReturn single(words)
        interactor.addWordsForLearning(chosenWords) leadsTo { wasWordChosen = true }

        wordsObserver = TestObserver()
        autoSlideObserver = TestObserver<Unit>()

        wasWordChosen = false
    }

    @Test fun loadingWords() {
        vm.load()
        vm.words().subscribe(wordsObserver) //can subscribe after getWords loaded
        wordsObserver.testOnNext(words)
        errorMessagesCount(0)
    }

    @Test fun wordsLoadedWithTroubles() {
        interactor.getWordsToLearn() mustReturn singleError()
        vm.load()
        vm.words().subscribe(wordsObserver)
        wordsObserver.testNoErrorsNoValues()
        errorMessagesCount(1)
    }

    @Test fun autoSlide() {
        vm.autoSlide().subscribe(autoSlideObserver)
        vm.wordPressed(MockedWords.wordAnt)
        vm.wordPressed(MockedWords.wordAnt) // don't slide when press the same word
        autoSlideObserver.assertValueCount(1)

        vm.wordPressed(MockedWords.wordCat)
        autoSlideObserver.assertValueCount(2)
    }

    @Test fun isWordChosenAndWordsAmount() = with(MockedWords) {

        allWords.forEach { word ->
            println("about eats assert !vm.isWordChosenAndWordsAmount(word) with word $word")
            assert(!vm.isWordChosen(word))
            vm.wordPressed(word)
        }

        vm.getWordsAmount().subscribe { amount ->
            assert(amount == allWords.size)
        }

        allWords.forEach { word ->
            println("about eats assert vm.isWordChosenAndWordsAmount(word) with word $word")
            assert(vm.isWordChosen(word))
        }
    }

    @Test fun end() {
        chosenWords.forEach { vm.wordPressed(it) }
        vm.end()
        assert(wasWordChosen)
    }
}