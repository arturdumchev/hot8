package com.livermor.hot8app.domain.learn_words.train_data_builders

import com.livermor.hot8app.common.mocks.MockedTrainData
import org.junit.Before
import org.junit.Test

class WritingDataBuilderTest {

    private val WORDS_PER_TRAIN = MockedTrainData.WORD_PER_TIME

    private val writingData = MockedTrainData.writingData
    private val listeningData = MockedTrainData.listeningData
    private val trainProgressList = MockedTrainData.wordsToOperate


    private val writingDataBuilder = WritingDataBuilder()

    @Before fun before() {
        MockedTrainData.cleanData()
    }

    @Test fun base() {
        val result = writingDataBuilder.getList(trainProgressList)
        val resultShouldBe = writingData + listeningData

        for (i in 0..resultShouldBe.size - 1) {
            println("result[i] = ${result[i]},\nshould[i] = ${resultShouldBe[i]}\n")
        }
        assert(result == resultShouldBe)
    }
}