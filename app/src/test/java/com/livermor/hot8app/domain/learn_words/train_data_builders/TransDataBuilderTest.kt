package com.livermor.hot8app.domain.learn_words.train_data_builders

import com.livermor.hot8app.common.mocks.MockedTrainData
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import com.livermor.hot8app.mock
import com.livermor.hot8app.mustReturn
import org.junit.Before
import org.junit.Test

class TransDataBuilderTest {

    companion object{
        fun assertEquals(data: TransData, data2: TransData) {
            assert(data.shouldContinue == data2.shouldContinue)
            assert(data.success == data2.success)
            assert(data.training == data2.training)
            assert(data.wordProgress == data2.wordProgress)
        }
    }

    private val WORDS_PER_TRAIN = MockedTrainData.WORD_PER_TIME

    private val wordRepo: IWordRepo = mock()
    private val transDataBuilder = TransDataBuilder(WORDS_PER_TRAIN, wordRepo)

    private val translations = MockedTrainData.translations
    private val trainProgressList = MockedTrainData.wordsToOperate

    @Before fun setUp() {
        wordRepo.getRandomWords(WORDS_PER_TRAIN * 2 - 2) mustReturn translations.take(WORDS_PER_TRAIN * 2 - 2)
        MockedTrainData.cleanData()
    }

    @Test fun getTranslations() = with(MockedTrainData) {
        val result = transDataBuilder.getList(trainProgressList)
        val resultListShouldBe = translationToWordData + wordsToTranslationData

        for (i in 0..resultListShouldBe.size - 1) {
            println("${result[i]}\n${resultListShouldBe[i]}")
            println()

            assertEquals(resultListShouldBe[i], result[i])
        }
    }
}