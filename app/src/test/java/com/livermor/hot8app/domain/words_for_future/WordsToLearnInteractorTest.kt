package com.livermor.hot8app.domain.words_for_future

import com.livermor.hot8app.data.preference.global.IGlobalData
import com.livermor.hot8app.common.mocks.GlobalDataMocked
import com.livermor.hot8app.common.mocks.MockedWords
import com.livermor.hot8app.data.repo.topics.ITopicsRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.mock
import org.junit.Before

class WordsToLearnInteractorTest {

    private val progressRepo: IProgressRepo = mock()
    private val topicsRepo: ITopicsRepo = mock()
    private val globalData: IGlobalData = GlobalDataMocked

    private val interactor = WordsInteractor(progressRepo, topicsRepo, globalData)

    @Before fun setUp() = with(MockedWords) {

    }

}