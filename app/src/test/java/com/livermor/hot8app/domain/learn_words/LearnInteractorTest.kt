package com.livermor.hot8app.domain.learn_words

import com.livermor.hot8app.common.mocks.MockedTrainData
import com.livermor.hot8app.common.mocks.ProgressRepoMocked
import com.livermor.hot8app.data.repo.words.IWordRepo
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.domain.ATTEMPTS_TO_LEARN
import com.livermor.hot8app.domain.WORDS_PACKS_TO_LEARN
import com.livermor.hot8app.domain.learn_words.train_data_builders.TransDataBuilderTest
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TrainData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import com.livermor.hot8app.mock
import com.livermor.hot8app.mustReturn
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test

class LearnInteractorTest {

    companion object {
        private val wordsAmount = MockedTrainData.WORD_PER_TIME
        private val translations = MockedTrainData.translations
        private val allTrainData = MockedTrainData.allTrainData
        private val baseWords = MockedTrainData.wordsToOperate
    }


    private var progressRepo: IProgressRepo = ProgressRepoMocked
    private var wordRepo: IWordRepo = mock()

    private val learnInteractor: ILearnInteractor = LearnInteractor(progressRepo, wordRepo, wordsAmount)
    private var trainDataObserver: TestObserver<List<TrainData>> = TestObserver()


    @Before fun setUp() {
        wordRepo.getRandomWords(wordsAmount * 2 - 2) mustReturn translations.take(wordsAmount * 2 - 2)
        trainDataObserver = TestObserver()
        MockedTrainData.cleanData()
    }

    @Test fun canSaveProgress() {

        (allTrainData[2] as ProgressData).success = true
        (allTrainData[3] as ProgressData).success = true
        (allTrainData[4] as ProgressData).attempt = ATTEMPTS_TO_LEARN
        (allTrainData[5] as ProgressData).attempt = ATTEMPTS_TO_LEARN

        learnInteractor.saveTrainedData(allTrainData)

        val progressWords = baseWords.filter { it.progress != 0 }.size
        assert(progressWords == 2)

        val errorWords = baseWords.filter { it.errors != 0 }.size
        assert(errorWords == 2)


    }

    @Test fun roundsControl() {
        assert(learnInteractor.haveDoneEnoughRounds(ATTEMPTS_TO_LEARN))
        (0..ATTEMPTS_TO_LEARN - 1).forEach { assert(!learnInteractor.haveDoneEnoughRounds(it)) }
    }

    @Test fun learnPacksControl() {
        assert(learnInteractor.haveLearnedEnoughPacks(WORDS_PACKS_TO_LEARN))
        (0..WORDS_PACKS_TO_LEARN - 1).forEach { assert(!learnInteractor.haveLearnedEnoughPacks(it)) }
    }


    @Test fun getFirstData() {
        println("progressRepo.getWordsInProgress() --> ${progressRepo.getWordsInProgress()}")

        learnInteractor.getTrainData(listOf()).subscribe(trainDataObserver)
        //trainDataObserver.testOnTerminate(firstResult)
        trainDataObserver.awaitTerminalEvent()

        val result = trainDataObserver.values().first()
        for (i in 0..result.size - 1) {
            println("${result[i]}\n${allTrainData[i]}\n\n")
            if (result[i] is TransData) {
                TransDataBuilderTest.assertEquals(allTrainData[i] as TransData, result[i] as TransData)
            } else assert(result[i] == allTrainData[i])
        }
    }

    @Test fun getSecondWordsSetWhenAllSuccess() {
        learnInteractor.getTrainData(allTrainData.map {
            if (it is ProgressData) it.success = true
            it
        }).subscribe(trainDataObserver)

        // should be empty list, since everything is success
        trainDataObserver.awaitTerminalEvent()
        val result = trainDataObserver.values().first()
        assert(!result.isEmpty())
        result.forEach {
            println(it)
            assert(!it.shouldContinue)
        }
    }


    @Test fun getWordsWhenHalfSuccess() {

        //1
        (allTrainData[3] as ProgressData).attempt = 1
        (allTrainData[3] as ProgressData).success = true
        //2
        (allTrainData[4] as ProgressData).attempt = 2
        // +3 (reading data)

        learnInteractor.getTrainData(allTrainData).subscribe(trainDataObserver)
        trainDataObserver.awaitTerminalEvent()
        trainDataObserver.assertValueAt(0, { result ->
            result.forEach(::println)
            println("result.filter{it.shouldContinue}.size ${result.filter { it.shouldContinue }.size}")
            result.filter { it.shouldContinue }.size == allTrainData.size - 3
        })
    }
}