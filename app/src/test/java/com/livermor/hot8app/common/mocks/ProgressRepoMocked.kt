package com.livermor.hot8app.common.mocks

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.data.repo.words_progress.IProgressRepo
import com.livermor.hot8app.data.repo.words_progress.TrainingType


object ProgressRepoMocked : IProgressRepo{
    override fun getWordsInProgress(): List<LearningProgress> {
        return MockedTrainData.wordsToOperate
    }

    override fun saveWordsToLearn(learningProgress: List<LearningProgress>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getClearWords(amount: Int): List<LearningProgress> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveIsLearned(word: LearningProgress, trainRaw: TrainingType) {
        word.progress = trainRaw.setTrue(word.progress)
    }

    override fun setErrorMask(word: LearningProgress, trainRaw: TrainingType, isError: Boolean) {
        val errorsData = word.errors
        val newErrData = if (isError) trainRaw.setTrue(errorsData) else trainRaw.setFalse(errorsData)
        word.errors = newErrData
    }
}