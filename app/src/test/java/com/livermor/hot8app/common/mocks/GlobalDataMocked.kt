package com.livermor.hot8app.common.mocks

import com.livermor.hot8app.data.preference.global.IGlobalData


object GlobalDataMocked : IGlobalData {
    override var userLevel: Int = 3
    override var wordsToLearn: Int = 0

    override var creationDateMillis: Long = System.currentTimeMillis()
    override var lastVisitDateMillis: Long = System.currentTimeMillis()
    override var visitsCount: Int = 0

}