package com.livermor.hot8app.common.mocks

import com.livermor.hot8app.data.LangLevel
import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.common.model.Word


object MockedWords {

    var wordsLevel = LangLevel.MIDDLE


    val wordCat = createWord("cat", "кошка")
    val wordDog = createWord("dog", "собачка")
    val wordMan = createWord("man", "мужичок")
    val wordFat = createWord("fat", "толстячок")
    val wordPet = createWord("pet", "питомчик")
    val wordAnt = createWord("ant", "муравьихо")
    val wordSon = createWord("son", "сынулька")
    val wordPen = createWord("pen", "писулька")

    val progressCat = LearningProgress(wordCat)
    val progressDog = LearningProgress(wordDog)
    val progressMan = LearningProgress(wordMan)
    val progressFat = LearningProgress(wordFat)

    val wordsFromTopics: List<Word> = listOf(wordDog, wordMan, wordFat, wordPet, wordAnt, wordSon, wordPen)
    val wordsInProgress: List<LearningProgress> = listOf(progressCat, progressDog, progressMan, progressFat)
    val wordsToChooseForLearning: List<Word> = wordsFromTopics - wordsInProgress.map { it.word }

    val allWords: List<Word> = wordsFromTopics + listOf(wordCat)

    private var idCounter = 0

    private fun createWord(word: String, trans: String): Word {
        return Word(idCounter++, trans, word, word, false, true, wordsLevel.num)
    }
}