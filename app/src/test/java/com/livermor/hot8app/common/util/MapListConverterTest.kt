package com.livermor.hot8app.common.util

import org.junit.Test

class MapListConverterTest {

    private val map = mapOf("a" to "b", "c" to "d")

    @Test fun test() = with(MapArrayConverter) {
        val list = getArr(map)
        val map2 = getMap(list)

        println(list)
        println(map2)
        assert(map == map2)
    }
}