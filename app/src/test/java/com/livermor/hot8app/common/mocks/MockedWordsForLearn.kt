package com.livermor.hot8app.common.mocks


object MockedWordsForLearn {
    val words = with(MockedWords) { listOf(wordAnt, wordCat, wordDog, wordDog) }
}