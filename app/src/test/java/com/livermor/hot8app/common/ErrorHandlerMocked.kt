package com.livermor.hot8app.common

import com.livermor.hot8app.common.error.CommonError
import com.livermor.hot8app.common.error.IErrorHandler
import com.livermor.hot8app.common.util.ps
import com.livermor.hot8app.common.util.eats
import io.reactivex.Observable


object ErrorHandlerMocked : IErrorHandler {

    const val TEST_MSG = "TEST_MSG"
    private val msgSub = ps<String>()

    override fun error(commonError: CommonError) {
        msgSub eats TEST_MSG
    }

    override fun error(throwable: Throwable) {
        msgSub eats TEST_MSG
    }

    override fun msgObservable(): Observable<String> = msgSub
}