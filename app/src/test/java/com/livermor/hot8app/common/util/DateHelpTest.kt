package com.livermor.hot8app.common.util

import com.livermor.hot8app.common.mocks.GlobalDataMocked
import org.junit.Before
import org.junit.Test

class DateHelpTest {

    companion object {
        const val DAY = 86400000L
        const val HOUR = 3600000L
    }

    private val currentDateZero: Long by lazy { System.currentTimeMillis() - System.currentTimeMillis() % DAY - HOUR * 3 }
    private val zeroDate: Long by lazy { currentDateZero }

    private lateinit var dateHelp: DateHelp
    private val globalData by lazy { GlobalDataMocked }

    private var lastVisitCount: Int = -1

    @Before fun setUp() {
        lastVisitCount = globalData.visitsCount
        dateHelp = DateHelp(globalData)
    }

    @Test fun theSameDate() {

        globalData.lastVisitDateMillis = zeroDate + 5 * HOUR
        dateHelp.update()
        assert(globalData.visitsCount == lastVisitCount)
    }

    @Test fun differentDate() {
        globalData.lastVisitDateMillis = zeroDate
        dateHelp.update()
        assert(globalData.visitsCount != lastVisitCount)
    }

}