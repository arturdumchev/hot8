package com.livermor.hot8app.common.mocks

import com.livermor.hot8app.common.model.LearningProgress
import com.livermor.hot8app.common.model.Word
import com.livermor.hot8app.domain.learn_words.Training
import com.livermor.hot8app.feature.learning.train_fragments.communication.ProgressData
import com.livermor.hot8app.feature.learning.train_fragments.communication.ReadingData
import com.livermor.hot8app.feature.learning.train_fragments.communication.TransData
import com.livermor.hot8app.feature.learning.train_fragments.communication.WritingData
import java.util.*


object MockedTrainData {

    val WORD_PER_TIME = 4

    val wordsToOperate = MockedWords.allWords.take(WORD_PER_TIME).map { it.intoLearningProgress() }
    val translations: List<Word> = MockedWords.allWords


    // train data
    val readingData = ReadingData(Training.READ_WORDS,
            wordsToOperate.fold(HashMap<String, String>()) { map, wordProgress ->
                map.apply { put(wordProgress.word.writing, wordProgress.word.translation) }
            })
    val translationToWordData = (0..WORD_PER_TIME - 1).map { createChooseWordWords(it) }
    val wordsToTranslationData = (0..WORD_PER_TIME - 1).map { createChooseTransWords(it) }
    val writingData = (0..WORD_PER_TIME - 1).map { createWritingData(it, true) }
    val listeningData = (0..WORD_PER_TIME - 1).map { createWritingData(it, false) }

    val allTrainData = listOf(readingData) + translationToWordData + wordsToTranslationData+ writingData + listeningData

    //——————————————————————————————————————————————————————————————————————
    fun cleanData() {
        allTrainData.forEach {
            if (it is ProgressData) {
                it.success = false
                it.wordProgress.progress = 0
                it.attempt = 0
            }
            it.shouldContinue = true
        }
        wordsToOperate.forEach {
            it.progress = 0
            it.errors = 0
        }
    }

    //——————————————————————————————————————————————————————————————————————

    private fun Word.intoLearningProgress() = LearningProgress(word = this)

    //—————————————————————————————————————————————————————————————————————— writing data creators

    private fun createWritingData(i: Int, writeNoListen: Boolean): WritingData {
        val word = wordsToOperate[i]
        return WritingData(if (writeNoListen) Training.WRITING else Training.LISTENING, word)
    }


    //—————————————————————————————————————————————————————————————————————— trans data creators

    private fun createChooseTransWords(i: Int): TransData {
        val word = wordsToOperate[i]
        return TransData(
                Training.TRANS_FROM_ENG,
                word,
                translations.take(WORD_PER_TIME).map(Word::translation))
    }

    private fun createChooseWordWords(i: Int): TransData {
        val word = wordsToOperate[i]
        return TransData(
                Training.TRANS_INTO_ENG,
                word,
                translations.drop(WORD_PER_TIME).map(Word::writing))
    }
}