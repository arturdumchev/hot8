package com.livermor.hot8app

import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.util.concurrent.TimeUnit


inline fun <reified T : Any> mock(): T = Mockito.mock(T::class.java)


infix fun <T> T.mustReturn(value: T) = `when`(this).thenReturn(value)
infix fun <T> T.leadsTo(action: () -> Unit) = `when`(this).then { action.invoke() }


const val TEST_ERROR = "test errorEmitter"
fun <T> single(t: T) = Single.just<T>(t)
fun <T> singleError(msg: String = TEST_ERROR) = Single.create<T> { it.onError(Throwable(msg)) }

//——————————————————————————————————————————————————————————————————————

fun <T> TestObserver<T>.testOnNext(t: T, time: Long = 10) {
    wait(time)
    assertValue { i -> i == t }
    assertNoErrors()
}

fun <T> TestObserver<T>.testOnTerminate(t: T) {
    awaitTerminalEvent()
    assertNoErrors()
    assertComplete()
    assertResult(t)
}

fun <T> TestObserver<T>.testErrorMsg(msg: String = TEST_ERROR) {
    awaitTerminalEvent()
    assertNotComplete()
    assertErrorMessage(msg)
}

fun <T> TestObserver<T>.testNoErrorsNoValues() {
    assertNoValues()
    assertNoErrors()
}

//——————————————————————————————————————————————————————————————————————

fun <T> TestObserver<T>.wait(t: Long) {
    await(t, TimeUnit.MILLISECONDS)
}

