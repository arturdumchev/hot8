package com.livermor.hot8app.data.repo.words_progress

import org.junit.Test

class WordProgressTest {

    var progressData = 0


    @Test fun save() {

        val words = TrainingType.values()
        for (word in words) {
            assert(word.getValue(progressData) == false)

            progressData = word.setTrue(progressData)
            assert(word.getValue(progressData) == true)

            progressData = word.setFalse(progressData)
            assert(word.getValue(progressData) == false)

            progressData = word.setFalse(progressData)
            assert(word.getValue(progressData) == false)
        }
    }

    @Test fun isEverythingClear() {
        var someData = 0
        assert(TrainingType.isAllTypesClear(someData))

        someData = TrainingType.CHOOSE_TRANSLATION.setTrue(someData)
        assert(!TrainingType.isAllTypesClear(someData))
    }
}